//
//  NotiMapViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 17/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "NotiMapViewController.h"
#import "ObjectMapAnnotation.h"
#import <AddressBookUI/AddressBookUI.h>

@interface NotiMapViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapMode;

@end

@implementation NotiMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"運單定位";
    //NSLog(@"latitude = %@, longitude = %@", self.latitude, self.longitude);
    self.navigationItem.rightBarButtonItem = nil;
    
    [self.mapView addAnnotations:[self createAnnotations]];
    
    [self createDropdownView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSMutableArray *)createAnnotations
{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    //Create coordinates from the latitude and longitude values
    CLLocationCoordinate2D coord;
    coord.latitude = (self.latitude!=(NSString *)[NSNull null])?self.latitude.doubleValue:0.0;
    coord.longitude = (self.longitude!=(NSString *)[NSNull null])?self.longitude.doubleValue:0.0;
    
    ObjectMapAnnotation *annotation = [[ObjectMapAnnotation alloc] initWithTitle:self.vhno AndCoordinate:coord];
    [annotations addObject:annotation];
    [self.mapView showAnnotations:annotations animated:YES];
    
    return annotations;
}


- (void) createDropdownView
{
    [self.vhno_label setText:self.vhno];
    NSString *time = (self.location_time!=(NSString*)[NSNull null])?self.location_time:@"未知";
    NSString *stateS =(self.state!=(NSString*)[NSNull null])?self.state:@"未知";
    [self.locationTime_label setText:[NSString stringWithFormat:@"%@ %@", stateS, time]];
    [self reverseGeocode];
    //[self.address_label setText:(self.address!=(NSString*)[NSNull null])?self.address:@"未知"];
    [self.odno_label setText:self.odno];
}


- (IBAction)changeMode:(UISegmentedControl *)sender {
    switch (self.mapMode.selectedSegmentIndex)
    {
        case 0:
            [self.mapView setMapType:MKMapTypeStandard];
            break;
        case 1:
            [self.mapView setMapType:MKMapTypeSatellite];
            break;
        default:
            break;
    }
    
}


- (void)reverseGeocode{
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
            self.address_label.text = @"網路連線錯誤，重新取得運單地址！";
            
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            self.address_label.text = [NSString stringWithFormat:@"%@", ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO)];
            //NSLog(@"%@", self.address_label.text);
        }
    }];
}


- (IBAction)backToPrevious:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
