//
//  NotiDetailViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 31/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) NSString *co;
@property (strong, nonatomic) NSString *div;
@property (strong, nonatomic) NSString *dldat;
@property (strong, nonatomic) NSString *form_number;

@property (strong, nonatomic) NSString *cuno;

-(void)getNumberData;


@end
