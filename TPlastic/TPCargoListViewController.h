//
//  TPCargoListViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 12/8/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationTracker.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>



@interface TPCargoListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property LocationTracker * locationTracker;


@property (weak, nonatomic) IBOutlet UIButton *selectAll;

@property (weak, nonatomic) IBOutlet UIButton *confirm_btn;

@property (weak, nonatomic) IBOutlet UIButton *selectState_btn;



- (BOOL)connected;

@end
