//
//  ObjectMapAnnotation.m
//  CRMFPG
//
//  Created by chenghsienyu on 17/11/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "ObjectMapAnnotation.h"

@implementation ObjectMapAnnotation

@synthesize coordinate=_coordinate;
@synthesize title=_title;
-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    _title = title;
    _coordinate = coordinate;
    return self;
}



@end
