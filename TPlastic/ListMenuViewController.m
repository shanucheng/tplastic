//
//  ListMenuViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "ListMenuViewController.h"


@interface ListMenuViewController ()

@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;

@end

@implementation ListMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"訂單與交運單查詢";
    
    [self customSetup];
    
    self.view_cos.hidden = NO;
    self.view_vh.hidden = YES;

    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem setTarget: self.revealViewController];
        [self.revealButtonItem setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        // Set the gesture
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segment:(id)sender {
    
    switch (self.segment.selectedSegmentIndex)
    {
        case 0:
            
            self.view_cos.hidden = NO;
            self.view_vh.hidden = YES;
            
            
            break;
        case 1:

            self.view_cos.hidden = YES;
            self.view_vh.hidden = NO;
            
            break;
        default:
            break;
    }
    
}



#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}

@end
