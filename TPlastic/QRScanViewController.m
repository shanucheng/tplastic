//
//  QRScanViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 09/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "QRScanViewController.h"
#import "TempData.h"

@interface QRScanViewController ()

@end

@implementation QRScanViewController

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self dispQRCodeCapture];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - QR Code Reader

- (void)dispQRCodeCapture {
    
    // Capture session init
    self.session = AVCaptureSession.new;
    
    AVCaptureDevice *backCameraDevice;
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if (device.position == AVCaptureDevicePositionBack) {
            backCameraDevice = device;
            break;
        }
    }
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCameraDevice
                                                                        error:&error];
    [self.session addInput:input];
    
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    
    AVCaptureMetadataOutput *output = AVCaptureMetadataOutput.new;
    [output setMetadataObjectsDelegate:self queue:dispatchQueue];
    [self.session addOutput:output];
    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    [self.session startRunning];
    
    self.previewView.backgroundColor = [UIColor clearColor];
    
    AVCaptureVideoPreviewLayer *avCapturePreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    avCapturePreviewLayer.frame = self.view.bounds;
    avCapturePreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [self.previewView.layer addSublayer:avCapturePreviewLayer];

}

#pragma mark - <AVCaptureMetadataOutputObjectsDelegate>

- (void)       captureOutput:(AVCaptureOutput *)captureOutput
    didOutputMetadataObjects:(NSArray *)metadataObjects
              fromConnection:(AVCaptureConnection *)connection {
    
    for (AVMetadataObject *metadata in metadataObjects) {
        
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            
            NSString *qrcode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            self.msg = qrcode;
            // 避免多次讀取
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            [self performSelectorOnMainThread:@selector(decodeMSG) withObject:nil waitUntilDone:NO];
            
            [self backToPrevious:self];
            
            break;
        }
    }
}


#pragma mark - strip msg
- (void) decodeMSG {
    
    if ([self.msg containsString:@"^$"]) {
        NSArray *tempArray = [self.msg componentsSeparatedByString: @"^$"];
        
        NSManagedObjectContext *context = [self managedObjectContext];
        NSError *error = nil;
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"TempData"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"vhno == %@", [tempArray objectAtIndex:0]];
        NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
        
        if (result.count == 0){
            
            NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"TempData" inManagedObjectContext:context];
            
            TempData *tempData = (TempData*)obj;
            
            tempData.vhno = [tempArray objectAtIndex:0];
            tempData.co = [tempArray objectAtIndex:1];
            tempData.div = [tempArray objectAtIndex:2];
            tempData.dldat = [tempArray objectAtIndex:3];
            tempData.isChecked = [NSNumber numberWithBool:YES];
            tempData.valid = [NSNumber numberWithBool:NO];
            tempData.stateID = @"0";
            
        }else{
            
            TempData *tempData = [result objectAtIndex:0];
            tempData.isChecked = [NSNumber numberWithBool:YES];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"重複運單"
                                                            message:@"運單已在清單之中，請重新掃描"
                                                           delegate:self
                                                  cancelButtonTitle:@"確定"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }

        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QRcode格式不符"
                                                 message:@"QRcode格式不符，請重新掃描"
                                                 delegate:self
                                                 cancelButtonTitle:@"確定"
                                                 otherButtonTitles:nil, nil];
        [alert show];
    }
}


- (IBAction)backToPrevious:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Action

- (void)stopReading{
    [self.session stopRunning];
    self.session = nil;
    
    [self.previewView removeFromSuperview];

    self.previewView = nil;
}

@end
