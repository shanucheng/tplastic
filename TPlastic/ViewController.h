//
//  ViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 12/8/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *toCargo_btn;
@property (weak, nonatomic) IBOutlet UIButton *toSearch;

@property (weak, nonatomic) IBOutlet UILabel *bundle_ver;

@end

