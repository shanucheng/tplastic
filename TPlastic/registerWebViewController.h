//
//  registerWebViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 21/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface registerWebViewController : UIViewController

@property (nonatomic, strong) NSString *urlString;
@property (nonatomic) BOOL isScaledToFit;

@end
