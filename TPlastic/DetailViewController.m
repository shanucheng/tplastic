//
//  DetailViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 01/04/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "DetailViewController.h"
#import "NotiMapViewController.h"

@interface DetailViewController ()<UITableViewDataSource, UITableViewDelegate, SwipeViewDataSource, SwipeViewDelegate>
{
    NSArray *itemList;
}

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *listItems;
@property (nonatomic, strong) UITableView *tableViewIn;

@property (nonatomic) NSInteger *indexNumber;


@end

@implementation DetailViewController
@synthesize tableViewIn;

- (void)dealloc
{
    self.swipeView.delegate = nil;
    self.swipeView.dataSource = nil;
}



-(void) preSetView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor blackColor].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    view.clipsToBounds = YES;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"運單明細";
    
    //UIImage *image = [UIImage imageNamed: @"ic_action_place.png"];
    
    //UIBarButtonItem *map_btn = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(toMapView:)];
    
    //UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(back:)];
    
    //self.navigationItem.rightBarButtonItem = map_btn;

    //self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:search, map_btn, nil];
    
    //configure swipeView
    _swipeView.pagingEnabled = YES;
    _swipeView.currentItemIndex = self.indexnum.row;
    [self.swipeView setCurrentItemIndex:self.indexnum.row];
    
    self.items = [NSMutableArray array];
    
    for (int i = 0; i < self.detailCard.count; i++) {
        [_items addObject:self.detailCard[i]];
    }
    _listItems = [self.list_data objectAtIndex:self.indexnum.row];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - SwipeView Delegate

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return _items.count;
}


- (void)swipeViewDidScroll:(SwipeView *)swipeView{
    
    self.indexNumber = swipeView.currentItemIndex;
    
    if ([[self.detailCard[swipeView.currentItemIndex] valueForKey:@"latitude"] isKindOfClass:[NSNull class]] || [[self.detailCard[swipeView.currentItemIndex] valueForKey:@"latitude"] isEqualToString:@"<null>"]) {
        //NSLog(@"null address");
        [self.map_btn setEnabled:NO];
        // NSLog(@"no map");
        
    }else{
        [self.map_btn setEnabled:YES];
        // NSLog(@"have map");
        
    }
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    
    NSLog(@"itemList = %@", _listItems);
    [swipeView reloadData];
    NSLog(@"swipe did changed");
    
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    //NSLog(@"index = %ld", (long)index);
    
    if ([[self.detailCard[index] valueForKey:@"latitude"] isKindOfClass:[NSNull class]] || [[self.detailCard[index] valueForKey:@"latitude"] isEqualToString:@"<null>"]) {
        //NSLog(@"null address");
        [self.map_btn setEnabled:NO];
        // NSLog(@"no map");
        
        
    }else{
        [self.map_btn setEnabled:YES];
        // NSLog(@"have map");
        
    }
    
    //_listItems = nil;
    
    UILabel *titleNum = nil;
    UIView *contentView = nil;
    UILabel *orderNum = nil;
    UILabel *vhnoNum = nil;
    UILabel *cuName = nil;
    UILabel *orderDate = nil;
    UILabel *dldate = nil;
    UILabel *salName = nil;
    UILabel *carName = nil;
    UILabel *state = nil;
    UILabel *locTime = nil;
    
    if (view == nil) {
        
        view = [[UIView alloc] initWithFrame:self.swipeView.bounds];
        //view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        titleNum = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y/2, view.frame.size.width, 36)];
        titleNum.textAlignment = NSTextAlignmentCenter;
        [titleNum setTag:1];
        [view addSubview:titleNum];
        
        //add content view
        contentView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width - 30, view.frame.size.height - 60)];
        [contentView setCenter:view.center];
        
        
        orderNum = [[UILabel alloc] initWithFrame:CGRectMake(contentView.frame.origin.x - 5, contentView.frame.origin.y - 30, contentView.frame.size.width, 36)];
        [contentView addSubview:orderNum];
        
        //add vhno
        vhnoNum = [[UILabel alloc] initWithFrame:CGRectMake(orderNum.frame.origin.x, orderNum.frame.origin.y +20, orderNum.frame.size.width, 36)];
        [contentView addSubview:vhnoNum];
        
        //add CUABR
        cuName = [[UILabel alloc] initWithFrame:CGRectMake(vhnoNum.frame.origin.x, vhnoNum.frame.origin.y +20, orderNum.frame.size.width, 36)];
        [contentView addSubview:cuName];
        
        //add AODAT
        orderDate = [[UILabel alloc] initWithFrame:CGRectMake(cuName.frame.origin.x, cuName.frame.origin.y +20, vhnoNum.frame.size.width, 36)];
        [contentView addSubview:orderDate];
        
        //add dldat
        dldate = [[UILabel alloc] initWithFrame:CGRectMake(orderDate.frame.origin.x, orderDate.frame.origin.y +20, cuName.frame.size.width, 36)];
        [contentView addSubview:dldate];
        
        //add SALMNM
        salName = [[UILabel alloc] initWithFrame:CGRectMake(dldate.frame.origin.x, dldate.frame.origin.y +20, cuName.frame.size.width, 36)];
        [contentView addSubview:salName];
        
        //add CARREBR
        carName = [[UILabel alloc] initWithFrame:CGRectMake(salName.frame.origin.x, salName.frame.origin.y +20, cuName.frame.size.width, 36)];
        [contentView addSubview:carName];
        
        //add state and location time
        state = [[UILabel alloc] initWithFrame:CGRectMake(carName.frame.origin.x, carName.frame.origin.y +30, contentView.frame.size.width/3+10, 32)];
        state.numberOfLines = 2;
        [state setTextAlignment:NSTextAlignmentCenter];
        [state setClipsToBounds:YES];
        [state.layer setCornerRadius:4.0f];
        [state setTextColor:[UIColor whiteColor]];
        [contentView addSubview:state];
        
        locTime = [[UILabel alloc] initWithFrame:CGRectMake(state.frame.origin.x + state.frame.size.width + 5, state.frame.origin.y, contentView.frame.size.width, 36)];
        [contentView addSubview:locTime];
        
        
        self.tableViewIn = [[UITableView alloc] initWithFrame:CGRectMake(view.frame.origin.x, state.frame.origin.y +50 ,contentView.frame.size.width, contentView.frame.size.height/2) style:UITableViewStylePlain];
        self.tableViewIn.dataSource = self;
        self.tableViewIn.delegate = self;
        self.tableViewIn.bounces = NO;

        [contentView addSubview:tableViewIn];
        
        
        
    }else{
        titleNum = (UILabel *)[view viewWithTag:1];
        
    }
    
    
    [titleNum setText:([_items[index] valueForKey:@"VHNO"]!=[NSNull null])?[_items[index] valueForKey:@"VHNO"]:@"未知"];
    
    [orderNum setText: [NSString stringWithFormat:@"訂單號碼 %@", ([_items[index] valueForKey:@"ODNO"]!=[NSNull null])?[_items[index] valueForKey:@"ODNO"]:@"未知"]];
    
    NSString *vhno = [NSString stringWithFormat:@"運單號碼 %@", ([_items[index] valueForKey:@"VHNO"]!=[NSNull null])?[_items[index] valueForKey:@"VHNO"]:@"未知"];
    [vhnoNum setText: vhno];
    
    NSString *cuabr = [NSString stringWithFormat:@"客戶名稱 %@", ([_items[index] valueForKey:@"CUABR"]!=[NSNull null])?[_items[index] valueForKey:@"CUABR"]:@"未知"];
    [cuName setText: cuabr];
    
    NSString *aodat = [NSString stringWithFormat:@"受訂日期 %@", ([_items[index] valueForKey:@"AODAT"]!=[NSNull null])?[_items[index] valueForKey:@"AODAT"]:@"未知"];
    [orderDate setText: aodat];
    
    NSString *dldat = [NSString stringWithFormat:@"交運日期 %@", ([_items[index] valueForKey:@"DLDAT"]!=[NSNull null])?[_items[index] valueForKey:@"DLDAT"]:@"未知"];
    [dldate setText: dldat];
    
    NSString *salmnm = [NSString stringWithFormat:@"營業員 %@", ([_items[index] valueForKey:@"SALMNM"]!=[NSNull null])?[_items[index] valueForKey:@"SALMNM"]:@"未知"];
    [salName setText: salmnm];
    
    NSString *carmnm = [NSString stringWithFormat:@"車行名稱 %@", ([_items[index] valueForKey:@"CARREBR"]!=[NSNull null])?[_items[index] valueForKey:@"CARREBR"]:@"未知"];
    [carName setText: carmnm];
    
    NSString *state_n = ([self.detailCard[index] valueForKey:@"state"]!=[NSNull null])?[self.detailCard[index] valueForKey:@"state"]:@"未知";
    [state setText: state_n];
    
    
    //add color later on
    if([state_n isEqualToString:@"已出發"]){
        state.backgroundColor = [UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f];
    }else if ([state_n isEqualToString:@"已抵達轉運站"]){
        state.backgroundColor = [UIColor colorWithRed:240/255.0f green:173/255.0f blue:78/255.0f alpha:1.0f];
    }else if ([state_n isEqualToString:@"已部份抵達"]){
        state.backgroundColor = [UIColor colorWithRed:91/255.0f green:192/255.0f blue:222/255.0f alpha:1.0f];
    }else if ([state_n isEqualToString:@"已抵達"]){
        state.backgroundColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f];
    }else if ([state_n isEqualToString:@"異常結案"]){
        state.backgroundColor = [UIColor colorWithRed:217/255.0f green:83/255.0f blue:79/255.0f alpha:1.0f];
    }else if ([state_n isEqualToString:@"未知"]){
        state.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f];
    }
    
    NSString *loctime = ([_items[index] valueForKey:@"locationTime"]!=[NSNull null])?[_items[index] valueForKey:@"locationTime"]:@"未知";
    [locTime setText:loctime];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _listItems = self.list_data[index];
        /*
         [self.tableViewIn beginUpdates];
         [self.tableViewIn deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationMiddle];
         [self.tableViewIn insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationMiddle];
         [self.tableViewIn endUpdates];
         */
    });
    
    [self preSetView:contentView];
    [view addSubview:contentView];
    
    
    
    return view;
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}


#pragma mark -
#pragma mark Table methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors =  [NSArray arrayWithObjects:(id)[UIColor lightGrayColor].CGColor, (id)[UIColor whiteColor].CGColor, nil];
    [cell.layer insertSublayer:gradient below:0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *item_label_txt = ([[_listItems objectAtIndex:indexPath.row] valueForKey:@"DSR"]!=[NSNull null])?[[_listItems objectAtIndex:indexPath.row] valueForKey:@"DSR"]:@"未知";
    ;
    NSString *item_qty_txt = ([[_listItems objectAtIndex:indexPath.row] valueForKey:@"DLQTY"]!=[NSNull null])?[[_listItems objectAtIndex:indexPath.row] valueForKey:@"DLQTY"]:@"未知";
    ;
    
    cell.textLabel.text = item_label_txt;
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.text = item_qty_txt;
    
    /*
     UILabel *item = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.size.width/2, cell.frame.size.height)];
     item.textAlignment = NSTextAlignmentCenter;
     item.layer.borderColor = [[UIColor grayColor] CGColor];
     item.layer.borderWidth = 0.5f;
     item.text = item_label_txt;
     
     UILabel *item_qty = [[UILabel alloc] initWithFrame:CGRectMake(item.frame.size.width, item.frame.origin.y, tableView.frame.size.width/2, cell.frame.size.height)];
     item_qty.textAlignment = NSTextAlignmentCenter;
     item_qty.layer.borderColor = [[UIColor grayColor] CGColor];
     item_qty.layer.borderWidth = 0.5f;
     item_qty.text = item_qty_txt;
     
     
     [cell.contentView addSubview:item];
     [cell.contentView addSubview:item_qty];
     */
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 88)];
    sectionHeaderView.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];;
    
    UILabel *headerLabel_1 = [[UILabel alloc] initWithFrame:
                              CGRectMake(0,0, sectionHeaderView.frame.size.width/2, 45)];
    UILabel *headerLabel_2 = [[UILabel alloc] initWithFrame:
                              CGRectMake(sectionHeaderView.frame.size.width/2,0, sectionHeaderView.frame.size.width/2, 45)];
    
    
    headerLabel_1.text = @"品名";
    [headerLabel_1 setTextColor:[UIColor whiteColor]];
    headerLabel_1.backgroundColor = [UIColor clearColor];
    headerLabel_1.textAlignment = NSTextAlignmentCenter;
    headerLabel_1.layer.borderColor = [[UIColor grayColor]CGColor];
    headerLabel_1.layer.borderWidth = 0.5f;
    
    headerLabel_2.text = @"數量";
    [headerLabel_2 setTextColor:[UIColor whiteColor]];
    headerLabel_2.backgroundColor = [UIColor clearColor];
    headerLabel_2.textAlignment = NSTextAlignmentCenter;
    headerLabel_2.layer.borderColor = [[UIColor grayColor]CGColor];
    headerLabel_2.layer.borderWidth = 0.5f;
    
    [sectionHeaderView addSubview:headerLabel_1];
    [sectionHeaderView addSubview:headerLabel_2];
    
    
    return sectionHeaderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

#pragma segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"toMap"]){
        NotiMapViewController *vc = segue.destinationViewController;
        NSDictionary *temp = [[NSDictionary alloc] init];
        temp = [self.detailCard objectAtIndex:self.indexNumber];
        vc.latitude = [temp valueForKey:@"latitude"];
        vc.longitude = [temp valueForKey:@"longitude"];
        vc.address = [temp valueForKey:@"address"];
        vc.location_time = [temp valueForKey:@"locationTime"];
        vc.vhno = [temp valueForKey:@"VHNO"];
        vc.odno = [temp valueForKey:@"ODNO"];
        vc.state = [temp valueForKey:@"state"];
    }
}

#pragma mark - IBAction

- (IBAction)back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)toMap:(id)sender {
    [self performSegueWithIdentifier:@"toMap" sender:self];
}


@end
