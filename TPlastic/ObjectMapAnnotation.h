//
//  ObjectMapAnnotation.h
//  CRMFPG
//
//  Created by chenghsienyu on 17/11/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface ObjectMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic,copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate;

@end
