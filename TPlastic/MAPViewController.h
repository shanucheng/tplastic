//
//  MAPViewController.h
//  CRMFPG
//
//  Created by chenghsienyu on 17/11/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface MAPViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *odno_label;
@property (weak, nonatomic) IBOutlet UILabel *vhno_label;
@property (weak, nonatomic) IBOutlet UILabel *locationTime_label;
@property (weak, nonatomic) IBOutlet UILabel *address_label;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *location_time;
@property (weak, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *vhno;
@property (strong, nonatomic) NSString *odno;
@property (strong, nonatomic) NSString *state;



@end
