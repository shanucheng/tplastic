//
//  CargoListTableViewCell.m
//  TPlastic
//
//  Created by chenghsienyu on 12/18/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "CargoListTableViewCell.h"

@implementation CargoListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    // Setup View
    [self setupView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
        
}

#pragma mark -
#pragma mark View Methods
- (void)setupView {
    
    [self.seleced_btn addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -
#pragma mark Actions

- (IBAction)tapButton:(id)sender {
    if (self.didTapButtonBlock) {
        self.didTapButtonBlock();
    }
}


@end
