//
//  VhSearchViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "VhSearchViewController.h"
#import "SVProgressHUD.h"
#import "Constants.h"
#import "SearchListViewController.h"
#import "ListMenuViewController.h"

@interface VhSearchViewController ()
{
    NSArray *formdata;
}

@end

@implementation VhSearchViewController



-(void) preSetView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    view.clipsToBounds = YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.api_key = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
    [self preSetView:self.tableView];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Keywords.plist"];
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    data = [[NSMutableArray alloc] init];
    
    self.search_textField.delegate = self;
    data = array;
    
    NSLog(@"keywords = %@", array);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)getFormListData: (NSString *)api number:(NSString *)numbers{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:NUMBER_URL@"&number=%@", api, numbers]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    //[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD show];
    [SVProgressHUD resetOffsetFromCenter];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *urlResponse, NSData * data, NSError *error) {
                               
                               NSData * jsonData = [NSData dataWithContentsOfURL:url];
                               NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                               if ([dataDictionary isKindOfClass:[NSDictionary class]] && [dataDictionary[@"data"] count] != 0) {
                                   //NSLog(@"data = %@", dataDictionary[@"data"]);
                                   formdata = dataDictionary[@"data"];
                                   /*
                                   NSMutableArray *newarray=[NSMutableArray array];
                                   for (NSDictionary *dictionary in dataDictionary[@"data"])
                                   {
                                       if (dictionary[@"CUABR"] != [NSNull null]) {
                                           [newarray addObject:dictionary];
                                       }
                                   }
                                   formdata = newarray;
                                   */
                                   [self.tableView reloadData];
                                   
                                   
                                   [SVProgressHUD dismiss];
                               }else if([dataDictionary[@"data"] count] == 0){
                                   //NSLog(@"error");
                                   [SVProgressHUD dismiss];
                               }
                           }];
}


#pragma mark MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    if ([textField isEqual:self.search_textField]) {
        return data;
    }else{
        return nil;
    }
}


- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(NSDictionary *)result
{
    //A selection was made - either by the user or by the textfield. Check if its a selection from the data provided or a NEW entry.
    if ([[result objectForKey:@"CustomObject"] isKindOfClass:[NSString class]] && [[result objectForKey:@"CustomObject"] isEqualToString:@"NEW"]) {
        //New Entry
    }else{
        //Selection from provided data
        if ([textField isEqual:self.search_textField]) {
            
            [self getFormListData:self.api_key number:self.search_textField.text];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.search_textField){
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        [self updateTextLabelsWithText: newString];
    }
    
    return YES;
}

-(void)updateTextLabelsWithText:(NSString *)string
{
    BOOL isLatin = [string canBeConvertedToEncoding:NSISOLatin1StringEncoding];
    if (string.length >=4 && isLatin) {
        [self getFormListData:self.api_key number:string];
    }
}
// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"Text field ended editing");
    if (textField == self.search_textField) {
        //[self getFormListData:self.api_key number:self.search_textField.text];
    }
}

// This method enables or disables the processing of return key
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

//touch event dismiss keyboard
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.search_textField resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}



// This method is called once we click inside the textField
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.search_textField){
        NSLog(@"numberfield did begin editing");
    }
}








#pragma mark UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return formdata.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"numberCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //NSLog(@"data = %@", formdata);

    NSString *testA = ([formdata[indexPath.row] valueForKey:@"form_number"]!=[NSNull null])?[formdata[indexPath.row] valueForKey:@"form_number"]:@"未知";
    NSString *testB = ([formdata[indexPath.row] valueForKey:@"CUABR"]!=[NSNull null])?[formdata[indexPath.row] valueForKey:@"CUABR"]:@"未知";
    NSString *testC = ([formdata[indexPath.row] valueForKey:@"CUNO"]!=[NSNull null])?[formdata[indexPath.row] valueForKey:@"CUNO"]:@"未知";
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", testA, testB];
    cell.detailTextLabel.text = testC;
    return cell;
}


- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath
{
    self.search_textField.text = [formdata[indexPath.row] valueForKey:@"form_number"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    // get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Keywords.plist"];
    //This copies objects of plist to array if there is one
    [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
    
    NSString *formNumber = [formdata[indexPath.row] valueForKey:@"form_number"];
    NSString *cuabr = [formdata[indexPath.row] valueForKey:@"CUABR"];
    NSString *cuno = [formdata[indexPath.row] valueForKey:@"CUNO"];
    
    //NSLog(@"num = %@, cubar = %@, cuno = %@", formNumber, cuabr, cuno);
    
    [array addObject: [NSDictionary dictionaryWithObjectsAndKeys:formNumber,@"DisplayText", cuabr,@"DisplaySubText", cuno,@"CustomObject",nil]];
    
    
    [array writeToFile:plistPath atomically: TRUE];

}


#pragma mark - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"numToList"]){
        SearchListViewController *vc = segue.destinationViewController;
        NSDictionary *temp = [formdata objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        vc.co = [temp valueForKey:@"CO"];
        vc.div = [temp valueForKey:@"DIV"];
        vc.type = [temp valueForKey:@"type"];
        vc.form_number = [temp valueForKey:@"form_number"];
        vc.dldat = [temp valueForKey:@"DLDAT"];
        vc.cuno = ([formdata[[self.tableView indexPathForSelectedRow].row] valueForKey:@"CUNO"]!=[NSNull null])?[formdata[[self.tableView indexPathForSelectedRow].row] valueForKey:@"CUNO"]:@"未知";
        vc.api_key = self.api_key;
        
        [vc getNumberData];
    }
}

@end
