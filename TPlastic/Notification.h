//
//  Notification.h
//  TPlastic
//
//  Created by chenghsienyu on 02/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notification : NSManagedObject

@property (nonatomic, retain) NSString * ad;
@property (nonatomic, retain) NSString * cn;
@property (nonatomic, retain) NSString * co;
@property (nonatomic, retain) NSString * dd;
@property (nonatomic, retain) NSString * div;
@property (nonatomic, retain) NSString * on;
@property (nonatomic, retain) NSString * vn;
@property (nonatomic, retain) NSString * lat;
@property (nonatomic, retain) NSString * lon;
@property (nonatomic, retain) NSString * sid;

@end
