//
//  CosSearchViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "CosSearchViewController.h"
#import "SVProgressHUD.h"
#import "shareInfo.h"
#import "Constants.h"
#import "SearchListViewController.h"
#import "customerID.h"

@interface CosSearchViewController ()
{
    NSArray *_numOfDays;
    UIActionSheet *pickerViewActionSheet;
    UIDatePicker *datePicker;
    UIToolbar *pickerToolbar;
    UIView *dateView;
}

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;


@end

@implementation CosSearchViewController

-(void) preSetView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    view.clipsToBounds = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //setup btn style
    self.intervalDays_btn.layer.masksToBounds = YES;
    self.intervalDays_btn.layer.cornerRadius = 5;
    self.intervalDays_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.startingDay_btn.layer.masksToBounds = YES;
    self.startingDay_btn.layer.cornerRadius = 5;
    self.startingDay_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.endingDay_btn.layer.masksToBounds = YES;
    self.endingDay_btn.layer.cornerRadius = 5;
    self.endingDay_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.confirm_btn.layer.masksToBounds = YES;
    self.confirm_btn.layer.cornerRadius = 5;
    self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.reset_btn.layer.masksToBounds = YES;
    self.reset_btn.layer.cornerRadius = 5;
    self.reset_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    //set segment index default
    self.segment_searchtype.selectedSegmentIndex = 0;
    self.segment_state.selectedSegmentIndex = 1;
    self.state = @"unclose";
    
    self.intervalDays_btn.hidden = NO;
    self.startingDay_btn.hidden = YES;
    self.endingDay_btn.hidden = YES;
    self.to_label.hidden = YES;
    
    
    //check id_field
    
    shareInfo *userInfo = [shareInfo sharedManager];
    NSString *id_field = userInfo.userInfo[@"idField"];
    if (![id_field isEqualToString:@"userName"]) {
        self.cos_label.hidden = YES;
        self.cos_textfield.hidden = YES;
    }
    
    
    //setup autocomplete
    [self.cos_textfield addTarget:self.autoCompleter action:@selector(textFieldValueChanged:)  forControlEvents:UIControlEventEditingChanged];
    [self preSetView:self.autoCompleter];

    //setup interval default
    [self DaysInterval:@"7"];
    
    [self.startingDay_btn setTitle:self.startingDate forState:UIControlStateNormal];
    [self.endingDay_btn setTitle:self.endingDate forState:UIControlStateNormal];

    
    [self.cos_textfield setText:@""];
    [self.cosID_label setText:@""];
    customerID *cos = [customerID sharedManager];
    cos.cuno = @"";
    cos.cuabr = @"";
    cos.DivName = @"";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - request cosID

- (void) getListOfCos{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *apiKey = [userDefaults objectForKey:@"apiKey"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:CUSTOMER_ID_URL, apiKey]];
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    //NSLog(@"%@", url);
    //[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    //[SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    //[SVProgressHUD show];
    dispatch_queue_t sendDeviceTokenQueue = dispatch_queue_create("get list", NULL);
    dispatch_async(sendDeviceTokenQueue, ^{
        NSData *returnData = [NSURLConnection sendSynchronousRequest:urlReq returningResponse:nil error:nil];
        //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        //NSLog(@"Provider returns: %@", returnString);
        if (!returnData) {
            return;
        }
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableContainers error: &error];
        if ([jsonObj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *jsonDict = (NSDictionary *) jsonObj;
                        
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([jsonDict[@"data"] isKindOfClass:[NSArray class]]) {
                    data = [[NSMutableArray alloc] init];

                    [jsonDict[@"data"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        [data addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[obj objectForKey:@"CUABR"] stringByAppendingString:[NSString stringWithFormat:@" %@ \n%@ %@ %@", [obj  objectForKey:@"CUNO"], [obj  objectForKey:@"divName"], [obj objectForKey:@"CO"], [obj objectForKey:@"DIV"]]], @"DisplayText", nil]];
                        
                    }];
                    self.cosList = [data valueForKey:@"DisplayText"];
                    //NSLog(@"%@", self.cosList);
                    //[SVProgressHUD dismiss];
                }else if ([[jsonDict allKeys] containsObject:@"error"]){
                    //[SVProgressHUD dismiss];
                    //NSLog(@"Token: %@", jsonDict[@"error"]);
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"錯誤訊息" message:[jsonDict[@"error"] valueForKey:@"keyword"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
                    [alert show];
                }
            });
        }
    });
}


- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:NO] forKey:ACOCaseSensitive];
        [options setValue:self.cos_textfield.font forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.cos_textfield inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        
        _autoCompleter.suggestionsDictionary = [NSArray arrayWithArray:self.cosList];
    }
    return _autoCompleter;
}

#pragma mark - AutoCompleteTableViewDelegate

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    
    return [NSArray arrayWithArray:self.cosList];
}

- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    //NSLog(@"%@ - Suggestion chosen: %d", completer, index);
}

#pragma mark - UItextfield delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField == self.cos_textfield){
        NSLog(@"%@", self.cos_textfield.text);
        [self.cos_textfield resignFirstResponder];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //get list of cos
    [self getListOfCos];
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.cos_textfield){
        //NSLog(@"%@", self.cos_textfield.text);
        [self.cos_textfield resignFirstResponder];
        customerID *cos = [customerID sharedManager];
        [self.cosID_label setText:[NSString stringWithFormat:@"%@（%@ %@）", cos.cuabr, cos.cuno, cos.DivName]];
    }
}
/*
//touch event dismiss keyboard
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.cos_textfield resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}*/


#pragma mark - segment control

- (IBAction)segment_state:(UISegmentedControl *)sender {
    
    switch (self.segment_state.selectedSegmentIndex)
    {
        case 0:
            self.state = @"all";
            break;
        case 1:
            self.state = @"unclose";
            break;
        case 2:
            self.state = @"closed";
            break;
        default:
            break;
    }

    
}

- (IBAction)segment_searchtype:(UISegmentedControl *)sender {
    
    switch (self.segment_searchtype.selectedSegmentIndex)
    {
        case 0:
            
            self.intervalDays_btn.hidden = NO;
            self.startingDay_btn.hidden = YES;
            self.endingDay_btn.hidden = YES;
            self.to_label.hidden = YES;
            
            break;
        case 1:
            
            self.intervalDays_btn.hidden = YES;
            self.startingDay_btn.hidden = NO;
            self.endingDay_btn.hidden = NO;
            self.to_label.hidden = NO;
            
            break;
        default:
            break;
    }
    
}

#pragma mark - intervalDays

- (void) DaysInterval: (NSString * )days{
    [self.intervalDays_btn setTitle:@"近 7 天" forState:UIControlStateNormal];
    self.intervalDays = days;
    int dayInt = [days intValue];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:-dayInt];
    NSDate *sevenDaysLate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, sevenDaysLate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //[self.startingDay_btn setTitle:[dateFormatter stringFromDate:currentDate] forState:UIControlStateNormal];
    //[self.endingDay_btn setTitle:[dateFormatter stringFromDate:sevenDaysLate] forState:UIControlStateNormal];
    self.startingDate = [dateFormatter stringFromDate:currentDate];
    self.endingDate = [dateFormatter stringFromDate:sevenDaysLate];
}

#pragma mark - select intervalDays

- (IBAction)selectDays_btn:(id)sender {
    NSMutableArray *myIntegers = [NSMutableArray array];
    shareInfo *userInfo = [shareInfo sharedManager];
    int limit_days = [userInfo.userInfo[@"limit_query_days"] intValue];
    
    for (NSInteger i = 1; i <= limit_days; i++)
        [myIntegers addObject:[NSNumber numberWithInteger:i]];
    _numOfDays = myIntegers;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"選擇天數" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
    UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 260, 100)];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    [myPickerView selectRow:6 inComponent:0 animated:YES];
    // Add picker to alert
    [alert setValue:myPickerView forKey:@"accessoryView"];
    
    NSString *dayNumbers = [[self.intervalDays_btn.titleLabel.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    [myPickerView selectRow:[dayNumbers integerValue]-1 inComponent:0 animated:NO];//fixed issue no.6
    //NSLog(@"%@", self.intervalDays_btn.titleLabel.text);
    [alert show];
    
}


#pragma mark - pickerview intervalDays

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    NSString *days = [NSString stringWithFormat:@"近 %@ 天", [_numOfDays[row] stringValue]];
    NSString *dayInt = [_numOfDays[row] stringValue];
    [self DaysInterval:dayInt];
    [self.intervalDays_btn setTitle:days forState:UIControlStateNormal];
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return _numOfDays.count;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [_numOfDays[row] stringValue];
}

#pragma mark - select starting & ending date

-(UIDatePicker *) startDatePicker{
    if (!_startDatePicker) {
        _startDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 320, 60)];
        _startDatePicker.datePickerMode = UIDatePickerModeDate;
        [_startDatePicker addTarget:self action:@selector(changeDateOfPicker:) forControlEvents:UIControlEventValueChanged];
        //_startDatePicker.date = [NSDate dateWithTimeInterval:-2592000 sinceDate:[NSDate date]];
        _startDatePicker.date = [NSDate date];//fixed current day issue
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.startingDate = [dateFormatter stringFromDate:[NSDate date]];
    }
    return _startDatePicker;
}

-(UIDatePicker *) stopDatePicker{
    if (!_stopDatePicker) {
        _stopDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 320, 60)];
        _stopDatePicker.datePickerMode = UIDatePickerModeDate;
        [_stopDatePicker addTarget:self action:@selector(changeDateOfPicker:) forControlEvents:UIControlEventValueChanged];
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:-7];
        NSDate *sevenDaysLate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        _stopDatePicker.date = sevenDaysLate;//fixed ending date issue
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.endingDate = [dateFormatter stringFromDate:sevenDaysLate];
        
    }
    return _stopDatePicker;
}

-(UIActionSheet *) startSheet{
    if (!_startSheet) {
        _startSheet = [[UIActionSheet alloc] initWithTitle:@"選擇查詢起始日期 \n\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    }
    return _startSheet;
}


-(UIActionSheet *) stopSheet{
    if (!_stopSheet) {
        _stopSheet = [[UIActionSheet alloc] initWithTitle:@"選擇查詢迄止日期 \n\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    }
    return _stopSheet;
}

-(void) startDatePickComplete:(UIButton *)sender{
    [self.startSheet dismissWithClickedButtonIndex:0 animated:YES];
}

- (IBAction)pickStartTime:(UIButton *)sender {
    if (([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending)) {
        // ios7
        [self.startSheet addSubview:self.startDatePicker];
        UIButton *done = [UIButton buttonWithType:UIButtonTypeSystem];
        [done setTitle:@"完成" forState:UIControlStateNormal];
        done.frame = CGRectMake(235, 5, 50, 30);
        [done addTarget:self action:@selector(startDatePickComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.startSheet addSubview:done];
        [self.startSheet showInView:self.view];
        [self changeDateOfPicker:self.startDatePicker];
        
    }
    else {
        // ios8
        self.alertController = [UIAlertController alertControllerWithTitle:@" 請選擇起始日期\n\n\n\n\n\n\n\n\n\n"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
        
        UISegmentedControl *closePicker = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"完成"]];
        closePicker.momentary = YES;
        closePicker.frame = CGRectMake(self.alertController.view.frame.origin.x +10 ,self.alertController.view.frame.origin.y + 10, 50.0f, 30.0f);
        closePicker.segmentedControlStyle = UISegmentedControlStyleBar;
        closePicker.tintColor = [UIColor blackColor];
        [closePicker addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
        [self.alertController.view addSubview:closePicker];
        [self.alertController.view addSubview:self.startDatePicker];
        [self presentViewController:self.alertController animated:YES completion:nil];
    }
    
}

- (void) dismissActionSheet{
    [self.alertController dismissViewControllerAnimated:YES completion:nil];
}

-(void) stopDatePickComplete:(UIButton *)sender{
    [self.stopSheet dismissWithClickedButtonIndex:0 animated:YES];
}


- (IBAction)pickStopTime:(UIButton *)sender {
    
    if (([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending)) {
        // ios7
        [self.stopSheet addSubview:self.stopDatePicker];
        UIButton *done = [UIButton buttonWithType:UIButtonTypeSystem];
        [done setTitle:@"完成" forState:UIControlStateNormal];
        done.frame = CGRectMake(235, 5, 50, 30);
        [done addTarget:self action:@selector(stopDatePickComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.stopSheet addSubview:done];
        
        [self.stopSheet showInView:self.view];
        [self changeDateOfPicker:self.stopDatePicker];
    }
    else {
        // ios8
        self.alertController = [UIAlertController alertControllerWithTitle:@" 請選擇結束日期\n\n\n\n\n\n\n\n\n\n"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
        
        UISegmentedControl *closePicker = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"完成"]];
        closePicker.momentary = YES;
        closePicker.frame = CGRectMake(self.alertController.view.frame.origin.x +10 ,self.alertController.view.frame.origin.y + 10, 50.0f, 30.0f);
        closePicker.segmentedControlStyle = UISegmentedControlStyleBar;
        closePicker.tintColor = [UIColor blackColor];
        [closePicker addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
        [self.alertController.view addSubview:closePicker];
        [self.alertController.view addSubview:self.stopDatePicker];
        [self presentViewController:self.alertController animated:YES completion:nil];
    }
    
}

-(void) changeDateOfPicker:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"Y-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:[datePicker date]];
    //    NSLog(@"%@", dateString);
    
    if ([datePicker isEqual:self.startDatePicker]) {
        [self.startingDay_btn setTitle:dateString forState:UIControlStateNormal];
        self.startingDate = dateString;
    } else {
        [self.endingDay_btn setTitle:dateString forState:UIControlStateNormal];
        self.endingDate = dateString;
    }
}

//iOS8.0+ when the date has chosen, remove the corresponding view from the parent view.
-(void) dateChosen:(UIBarButtonItem *) barButton {
    if(barButton.tag ==123){
        [dateView removeFromSuperview];
        
    }
    
}

//Get the chosen date from the UIDatePicker to the corresponding UITextField.
-(void)dateChanged{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //self.dateOfBirthTextField.text = [dateFormatter stringFromDate:[datePicker date]];
}

//Pre iOS8.0 for closing the actionsheet
-(BOOL)closeDatePicker:(id)sender{
    [pickerViewActionSheet dismissWithClickedButtonIndex:0 animated:YES];
    //[self.dateOfBirthTextField resignFirstResponder];
    return YES;
}

//Pre iOS8.0
-(void)doneButtonClicked{
    [self closeDatePicker:self];
}

#pragma mark - IBAction

- (IBAction)confirm:(id)sender {
    shareInfo *userInfo = [shareInfo sharedManager];
    NSString *id_field = userInfo.userInfo[@"idField"];
    if ([id_field isEqualToString:@"userName"]) {
        if (self.cos_textfield.text.length == 0 || [self.cos_textfield.text isEqualToString:@"不限"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入客戶代號" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
            [alert show];
        }else{
            
            [self performSegueWithIdentifier:@"toListView" sender:self];
        }
    }else{
        [self performSegueWithIdentifier:@"toListView" sender:self];
    }
    
}


- (IBAction)reset:(id)sender {
    [self DaysInterval:@"7"];
    [self.intervalDays_btn setTitle:@"近 7 天" forState:UIControlStateNormal];
    [self.startingDay_btn setTitle:self.startingDate forState:UIControlStateNormal];
    [self.endingDay_btn setTitle:self.endingDate forState:UIControlStateNormal];
    [self.cos_textfield setText:@""];
    [self.cosID_label setText:@""];
    self.segment_state.selectedSegmentIndex = 1;
    self.state = @"unclose";
    customerID *cos = [customerID sharedManager];
    cos.cuno = @"";
    cos.cuabr = @"";
    cos.DivName = @"";
    
}


#pragma mark - segue perform
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toListView"]) {
        shareInfo *userInfo = [shareInfo sharedManager];
        NSString *id_field = userInfo.userInfo[@"idField"];
        if ([id_field isEqualToString:@"userName"]) {
            SearchListViewController *vc = segue.destinationViewController;
            vc.cuno = self.cos_textfield.text;
            vc.startDate = self.startingDate;
            vc.endDate = self.endingDate;
            vc.isclosed = self.state;
            
            customerID *cos = [customerID sharedManager];
            vc.div = cos.div;
            vc.co = cos.co;
            [vc getCustomerData];
        }else{
            SearchListViewController *vc = segue.destinationViewController;
            vc.cuno = userInfo.userInfo[@"CUNO"];
            vc.div = userInfo.userInfo[@"DIV"];
            vc.co = userInfo.userInfo[@"CO"];
            vc.startDate = self.startingDate;
            vc.endDate = self.endingDate;
            vc.isclosed = self.state;
            [vc getCustomerData];
        }
    }
    
}




@end
