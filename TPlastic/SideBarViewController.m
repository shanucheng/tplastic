//
//  SideBarViewController.m
//  CRMFPG
//
//  Created by chenghsienyu on 08/10/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "SideBarViewController.h"
#import "shareInfo.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@implementation SWUITableViewCell
@end

@implementation SideBarViewController

/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    menuItems = @[@"sectionSearch", @"notification", @"ik"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

*/

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Extra

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender{
    
    if ([segue.identifier isEqualToString:@"logout"]) {
        
        if (![self connected]) {
            // not connected
            //NSLog(@"no internet");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無網路連線"
                                                            message:@"請檢查網路連線狀態"
                                                           delegate:self
                                                  cancelButtonTitle:@"確定"
                                                  otherButtonTitles:nil, nil];
            
            [alert show];
            
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"autologin"];
            //NSLog(@"logout");
            NSString * apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
            NSLog(@"apikey = %@", apiKey);
            
            NSString *urlString = [NSString stringWithFormat:LOGOUT_URL@"?api-key=%@",apiKey];
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
            NSLog(@"%@", url);
            [SVProgressHUD show];
            [NSURLConnection sendAsynchronousRequest:urlRequest
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *urlResponse, NSData * data, NSError *error) {
                                       NSData * jsonData = [NSData dataWithContentsOfURL:url];
                                       NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                                       //NSLog(@"result = %@", dataDictionary);
                                       if(dataDictionary[@"success"]){
                                           
                                           NSMutableArray *array = [[NSMutableArray alloc] init];
                                           // get paths from root direcory
                                           NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                                           // get documents path
                                           NSString *documentsPath = [paths objectAtIndex:0];
                                           // get the path to our Data/plist file
                                           NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Keywords.plist"];
                                           //This copies objects of plist to array if there is one
                                           [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
                                           
                                           [array removeAllObjects];
                                           
                                           [array writeToFile:plistPath atomically: TRUE];
                                           
                                           //off notification
                                           //[[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeNone];
                                           
                                           [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"notification"];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                           
                                           [SVProgressHUD dismiss];
                                           
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:dataDictionary[@"success"]
                                                                                           message:nil
                                                                                          delegate:self
                                                                                 cancelButtonTitle:@"確定"
                                                                                 otherButtonTitles:nil, nil];
                                           
                                           [alert show];
                                           
                                           
                                       }else{
                                           
                                           [SVProgressHUD dismiss];
                                           
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:dataDictionary[@"error"]
                                                                                           message:nil
                                                                                          delegate:self
                                                                                 cancelButtonTitle:@"確定"
                                                                                 otherButtonTitles:nil, nil];
                                           
                                           [alert show];
                                           
                                           
                                       }
                                       
                                       
                                   }];
            
        }
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"Cell";
    
    switch ( indexPath.row )
    {
        case 0:
            CellIdentifier = @"sectionSearch";
            break;
            
        case 1:
            CellIdentifier = @"notification";
            break;
            
        case 2:
            CellIdentifier = @"ik";
            break;
        case 3:
            CellIdentifier = @"back";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    
    return cell;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 88)];
    sectionHeaderView.backgroundColor = [UIColor blackColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(15,0, sectionHeaderView.frame.size.width, 88)];
    
    shareInfo *sharemodel = [shareInfo sharedManager];
    NSDictionary *userInfo = [[NSDictionary alloc] init];
    userInfo = sharemodel.userInfo;
    
    //NSLog(@"user = %@", sharemodel.userInfo);
    
    
    if (![userInfo[@"idField"] isEqual:@"userName"]) {
        NSString *CUNO = userInfo[@"CUNO"];
        NSString *username = userInfo[@"userName"];
        NSString *rolename = userInfo[@"roleName"];
        NSString *name = userInfo[@"name"];
        
        headerLabel.text = [NSString stringWithFormat:@"%@\n%@  %@ %@ Ver.%@", username, rolename, name, CUNO, [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]];
        [headerLabel setTextColor:[UIColor whiteColor]];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textAlignment = NSTextAlignmentLeft;
        headerLabel.numberOfLines = 2;
        
        [sectionHeaderView addSubview:headerLabel];
        
    }else {
        NSString *username = userInfo[@"userName"];
        NSString *rolename = userInfo[@"roleName"];
        NSString *name = userInfo[@"name"];
        
        headerLabel.text = [NSString stringWithFormat:@"%@\n%@  %@", username, rolename, name];
        [headerLabel setTextColor:[UIColor whiteColor]];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textAlignment = NSTextAlignmentLeft;
        headerLabel.numberOfLines = 2;
        
        [sectionHeaderView addSubview:headerLabel];
    }
    
    
    
    
    return sectionHeaderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 88;
}


#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

@end

