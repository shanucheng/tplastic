//
//  TempData.h
//  TPlastic
//
//  Created by chenghsienyu on 11/02/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TempData : NSManagedObject

@property (nonatomic, retain) NSString * co;
@property (nonatomic, retain) NSString * div;
@property (nonatomic, retain) NSString * dldat;
@property (nonatomic, retain) NSNumber * isChecked;
@property (nonatomic, retain) NSString * stateID;
@property (nonatomic, retain) NSString * vhno;
@property (nonatomic, retain) NSNumber * valid;

@end
