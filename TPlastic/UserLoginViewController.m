//
//  UserLoginViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 20/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "UserLoginViewController.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "registerWebViewController.h"
#import "shareInfo.h"

#import "AppDelegate.h"

@interface UserLoginViewController ()<UITextFieldDelegate>



@end

@implementation UserLoginViewController

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"使用者登入";
    
    
    self.login_btn.layer.masksToBounds = YES;
    self.login_btn.layer.cornerRadius = 5;
    self.login_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    self.register_btn.layer.masksToBounds = YES;
    self.register_btn.layer.cornerRadius = 5;
    self.register_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    NSString *rempwd = [[NSUserDefaults standardUserDefaults] valueForKey:@"rempwd"];
    
    if ([rempwd isEqualToString:@"YES"]) {
        [self.remPwd_btn setSelected:YES];
        NSString *userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"userID"];
        NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"password"];
        self.pwd_textfield.text = password;
        self.userID_textfield.text = userID;
        self.pwd_textfield.secureTextEntry = YES;
        
    }else{
        [self.remPwd_btn setSelected:NO];
        self.userID_textfield.text = @"NoteID/客戶編號";
        self.pwd_textfield.text = @"密碼";
    }
    
    
    NSString *loginState = [[NSUserDefaults standardUserDefaults]valueForKey:@"autologin"];
    
    if ([loginState isEqualToString:@"YES"]) {
        [self.autoLogin_btn setSelected:YES];
        [self performSelectorOnMainThread:@selector(login_btn:) withObject:nil waitUntilDone:YES];

    }else{
        [self.autoLogin_btn setSelected:NO];
    }
    
    
    NSString *n = [[NSUserDefaults standardUserDefaults] valueForKey:@"notification"];
    NSLog(@"n = %@", n);
    if ([n isEqualToString:@"on"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_start"];
        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_arrival"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - general alert

- (void) alertStatus:(NSString *)msg :(NSString *)title :(int) tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
}


#pragma mark - UItextfield delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField == self.userID_textfield){
        NSLog(@"%@", self.userID_textfield.text);
    }else if (textField == self.pwd_textfield){
        NSLog(@"%@", self.pwd_textfield.text);
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.pwd_textfield){
        self.pwd_textfield.secureTextEntry = YES;
    }
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.userID_textfield resignFirstResponder];
    [self.pwd_textfield resignFirstResponder];
    if ([self.userID_textfield.text isEqualToString:@""] || [self.pwd_textfield.text isEqualToString:@""]) {
        [self.remPwd_btn setSelected:NO];
        [self.autoLogin_btn setSelected:NO];
    }
}
//touch event dismiss keyboard
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.userID_textfield resignFirstResponder];
    [self.pwd_textfield resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}





#pragma mark - segue_perform
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toRevealView"]) {
        
    }else if ([segue.identifier isEqualToString:@"toRegisterWebView"]) {
        registerWebViewController *vc = segue.destinationViewController;
        vc.urlString = APPLYING_URL;
        vc.isScaledToFit = YES;
    }
}


#pragma mark - IBAction

- (IBAction)remPwd_btn:(id)sender {
    
    if ([self.userID_textfield.text isEqualToString:@"NoteID/客戶編號"] || [self.pwd_textfield.text isEqualToString:@"密碼"] || [self.userID_textfield.text isEqualToString:@""] || [self.pwd_textfield.text isEqualToString:@""]) {
        [self alertStatus:nil :@"請輸入帳號密碼" :0];
    }else{
        
        if (self.remPwd_btn.selected) {
            self.userID_textfield.text = @"NoteID/客戶編號";
            self.pwd_textfield.text = @"密碼";
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"rempwd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.remPwd_btn setSelected:NO];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"rempwd"];
            [[NSUserDefaults standardUserDefaults] setValue:self.userID_textfield.text forKey:@"userID"];
            [[NSUserDefaults standardUserDefaults] setValue:self.pwd_textfield.text forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.remPwd_btn setSelected:YES];
        }
        
    }
}

- (IBAction)autoLogin_btn:(id)sender {
    
    if ([self.userID_textfield.text isEqualToString:@"NoteID/客戶編號"] || [self.pwd_textfield.text isEqualToString:@"密碼"] || [self.userID_textfield.text isEqualToString:@""] || [self.pwd_textfield.text isEqualToString:@""]) {
        
        [self alertStatus:nil :@"請輸入帳號密碼" :0];
    
    }else{
     
        if (self.autoLogin_btn.selected) {
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"autologin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.autoLogin_btn setSelected:NO];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"autologin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.autoLogin_btn setSelected:YES];
        }
        
    }

}

- (IBAction)login_btn:(id)sender {
    
    NSLog(@"uID = %@, pwd = %@", self.userID_textfield.text, self.pwd_textfield.text);
    
    if (![self connected]) {
        // not connected
        //NSLog(@"no internet");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無網路連線"
                                                        message:@"請檢查網路連線狀態"
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
        
    }else{
        

    if ([self.userID_textfield.text isEqualToString:@""] || [self.pwd_textfield.text isEqualToString:@""] || [self.userID_textfield.text isEqualToString:@"NoteID/客戶編號"] ||  [self.pwd_textfield.text isEqualToString:@"密碼"]) {
        [self alertStatus:nil :@"請輸入帳號密碼" :0];
    }else{
        NSString * apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
        NSLog(@"currentapikey = %@", apiKey);
        NSString *post =[[NSString alloc] initWithFormat:@"userName=%@&password=%@&api-key=%@",[self.userID_textfield text], [self.pwd_textfield text], apiKey];
        
        //NSLog(@"PostData: %@",post);
        
        NSURL *url=[NSURL URLWithString:LOGIN_URL];
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
        //[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
        [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
        [SVProgressHUD show];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %ld", (long)[response statusCode]);
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([response statusCode] >= 200 && [response statusCode] < 300){
                    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    NSLog(@"Response ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    if (jsonData[@"success"]) {
                        NSLog(@"result = %@", jsonData[@"success"]);
                        NSDictionary *userInfo = jsonData[@"userInfo"];
                        
                        shareInfo *shareModel = [shareInfo sharedManager];
                        shareModel.userInfo = userInfo;
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"notification"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [SVProgressHUD dismiss];
                        
                        [self performSegueWithIdentifier:@"toRevealView" sender:self];
                        
                    }else{
                        NSLog(@"result = %@", jsonData[@"error"]);
                        [self alertStatus:jsonData[@"error"] : @"登入失敗" :1];
                        [SVProgressHUD dismiss];
                    }
                }else{
                    
                    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    NSLog(@"Response ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    NSLog(@"result = %@", jsonData[@"error"]);

                    if ([jsonData[@"error"] isEqualToString:@"Invalid API Key "]) {
                        NSLog(@"error = %@",jsonData[@"error"]);
                        //add method later
                        [self alertStatus:@"重新註冊取得API Key" : @"完成後請重新登入" :1];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"apiKey"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSLog(@"markAPIKey = %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"]);
                        //[[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeNone];
                        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
                        
                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
                            //Right, that is the point
                            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                                                 |UIRemoteNotificationTypeSound
                                                                                                                 |UIRemoteNotificationTypeAlert) categories:nil];
                            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
                        } else {
                            //register to receive notifications
                            
                            UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
                            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
                        }
                        
                        //[self performSelectorOnMainThread:@selector(login_btn:) withObject:nil waitUntilDone:YES];
                    }else{
                        [self alertStatus:jsonData[@"error"] : @"登入失敗" :1];
                    }
                    [SVProgressHUD dismiss];
                }
            });
        });

    }
    }
    
}

- (IBAction)register_btn:(id)sender {
    
    [self performSegueWithIdentifier:@"toRegisterWebView" sender:self];
    
}




@end
