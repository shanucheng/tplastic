//
//  QRScanViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 09/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface QRScanViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, strong) AVCaptureSession *session;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (nonatomic, strong) NSString *msg;

@end
