//
//  CosSearchViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutocompletionTableView.h"

@protocol AutocompletionTableViewDelegate;


@interface CosSearchViewController : UIViewController <AutocompletionTableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIPickerViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate>
{
    NSMutableArray *data;
}

@property (weak, nonatomic) IBOutlet UIButton *intervalDays_btn;

@property (weak, nonatomic) IBOutlet UIButton *startingDay_btn;
@property (weak, nonatomic) IBOutlet UIButton *endingDay_btn;
@property (weak, nonatomic) IBOutlet UIButton *confirm_btn;
@property (weak, nonatomic) IBOutlet UIButton *reset_btn;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_state;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_searchtype;

@property (weak, nonatomic) IBOutlet UILabel *cos_label;
@property (weak, nonatomic) IBOutlet UITextField *cos_textfield;

@property (strong, nonatomic) NSArray *cosList;

@property (strong, nonatomic) NSString *intervalDays;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *startingDate;
@property (strong, nonatomic) NSString *endingDate;

@property (nonatomic, strong) UIDatePicker *startDatePicker;
@property (nonatomic, strong) UIDatePicker *stopDatePicker;
@property (nonatomic, strong) UIActionSheet *startSheet;
@property (nonatomic, strong) UIActionSheet *stopSheet;
@property (nonatomic, strong) UIAlertController *alertController;

@property (weak, nonatomic) IBOutlet UILabel *to_label;

@property (weak, nonatomic) IBOutlet UILabel *cosID_label;

@end
