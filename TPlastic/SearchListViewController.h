//
//  SearchListViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 23/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *co;
@property (strong, nonatomic) NSString *div;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *dldat;
@property (strong, nonatomic) NSString *form_number;

@property (strong, nonatomic) NSString *cuno;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *api_key;
@property (strong, nonatomic) NSString *isclosed;
@property (strong, nonatomic) NSString *logisticName;

@property (strong, nonatomic) NSString *color;



-(void)getCustomerData;
-(void)getNumberData;

@end
