//
//  NotificationTableViewCell.h
//  TPlastic
//
//  Created by chenghsienyu on 16/02/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *odno;
@property (weak, nonatomic) IBOutlet UILabel *vhno;
@property (weak, nonatomic) IBOutlet UILabel *aodat;
@property (weak, nonatomic) IBOutlet UILabel *stateID;

@property (weak, nonatomic) IBOutlet UIButton *map_btn;

@end
