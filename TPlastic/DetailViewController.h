//
//  DetailViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 01/04/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSArray *list_data;
@property (nonatomic, strong) NSDictionary *detailData;
@property (nonatomic, strong) NSArray *detailCard;
@property (nonatomic, strong) NSIndexPath *indexnum;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *map_btn;


@end
