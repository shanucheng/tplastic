//
//  ListTableViewCell.h
//  TPlastic
//
//  Created by chenghsienyu on 23/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *number_extra;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *hoster;
@property (weak, nonatomic) IBOutlet UILabel *logisticName;
@property (weak, nonatomic) IBOutlet UILabel *locationTime;
@property (weak, nonatomic) IBOutlet UILabel *prograss;

@property (weak, nonatomic) IBOutlet UIButton *Map;

@end
