//
//  NotificationSettingViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 09/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "NotificationSettingViewController.h"

@interface NotificationSettingViewController ()

@end

@implementation NotificationSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSString *n = [[NSUserDefaults standardUserDefaults] valueForKey:@"notification"];
    NSString *st = [[NSUserDefaults standardUserDefaults] valueForKey:@"state_start"];
    NSString *sar = [[NSUserDefaults standardUserDefaults] valueForKey:@"state_arrival"];
    
    
    NSLog(@"%@ %@ %@", n, st, sar);
    
    
    if ([n isEqualToString:@"on"]) {
        [self.noti_switch setOn:YES];
    }else{
        [self.noti_switch setOn:NO];
    }
    
    if ([st isEqualToString:@"on"]) {
        [self.state_start setOn:YES];
    }else{
        [self.state_start setOn:NO];
    }
    
    if ([sar isEqualToString:@"on"]) {
        [self.state_arrival setOn:YES];
    }else{
        [self.state_arrival setOn:NO];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) switchToggled:(id)sender {
    
    if ([self.noti_switch isOn]) {
        NSLog(@"its on!");
        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"notification"];
        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_start"];
        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_arrival"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.state_arrival setOn:YES animated:YES];
        [self.state_start setOn:YES animated:YES];
    } else {
        NSLog(@"its off!");
        [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"notification"];
        [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_start"];
        [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_arrival"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.state_arrival setOn:NO animated:YES];
        [self.state_start setOn:NO animated:YES];
    }
}

- (IBAction)notification:(id)sender {
    
    [self switchToggled:self];
    
}

- (IBAction)state_stat:(id)sender {
    if([self.noti_switch isOn]){
        if([self.state_start isOn]){
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_start"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_start"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        if([self.state_start isOn]){
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_start"];
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"notification"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.noti_switch setOn:YES animated:YES];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_start"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
}


- (IBAction)state_arri:(id)sender {
    if([self.noti_switch isOn]){
        if([self.state_arrival isOn]){
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_arrival"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_arrival"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        if([self.state_arrival isOn]){
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"state_arrival"];
            [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"notification"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.noti_switch setOn:YES animated:YES];
        }else{
            [[NSUserDefaults standardUserDefaults] setValue:@"off" forKey:@"state_arrival"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (IBAction)vibrate:(id)sender {
    if([self.noti_switch isOn]){
        
        if ([self.vibrate isOn]) {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }else{
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        }
        
    }else{
        
        [self.vibrate setOn:NO];
        
    }
    
    
    
}


@end
