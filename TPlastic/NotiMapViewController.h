//
//  NotiMapViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 17/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface NotiMapViewController : UIViewController

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *odno_label;
@property (weak, nonatomic) IBOutlet UILabel *vhno_label;
@property (weak, nonatomic) IBOutlet UILabel *locationTime_label;
@property (weak, nonatomic) IBOutlet UILabel *address_label;

@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *location_time;
@property (weak, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *vhno;
@property (strong, nonatomic) NSString *odno;
@property (strong, nonatomic) NSString *state;

@end
