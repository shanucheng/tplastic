//
//  ListMenuViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListMenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

@property (weak, nonatomic) IBOutlet UIView *view_cos;
@property (weak, nonatomic) IBOutlet UIView *view_vh;

@end
