//
//  Temp.h
//  TPlastic
//
//  Created by chenghsienyu on 13/01/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Temp : NSManagedObject

@property (nonatomic, retain) NSString * keyword;

@end
