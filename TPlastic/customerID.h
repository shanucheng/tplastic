//
//  customerID.h
//  TPlastic
//
//  Created by chenghsienyu on 23/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface customerID : NSObject
{
    NSString *cuabr;
    NSString *cuno;
    NSString *DivName;
    NSString *co;
    NSString *div;
}


@property (nonatomic, retain) NSString *cuabr;
@property (nonatomic, retain) NSString *cuno;
@property (nonatomic, retain) NSString *DivName;
@property (nonatomic, retain) NSString *co;
@property (nonatomic, retain) NSString *div;

+ (id)sharedManager;
@end
