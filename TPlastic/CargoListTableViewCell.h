//
//  CargoListTableViewCell.h
//  TPlastic
//
//  Created by chenghsienyu on 12/18/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^TSPToDoCellDidTapButtonBlock)();

@interface CargoListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *state_button;

@property (weak, nonatomic) IBOutlet UIButton *seleced_btn;

@property (weak, nonatomic) IBOutlet UILabel *vhno_Label;
@property (weak, nonatomic) IBOutlet UILabel *dldat_Label;

@property (copy, nonatomic) TSPToDoCellDidTapButtonBlock didTapButtonBlock;

@end
