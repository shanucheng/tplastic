//
//  NotiDetailViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 31/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "NotiDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "SVProgressHUD.h"
#import "NotiDetailTableViewCell.h"
#import "NotiMapViewController.h"
#import "DetailViewController.h"


@interface NotiDetailViewController ()

@property (strong, nonatomic) NSArray *ListTableData;
@property (strong, nonatomic) NSString *location;
@property (weak, nonatomic) NSIndexPath *mapIndex;

@end

@implementation NotiDetailViewController

-(void) preSetView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.backgroundColor = [UIColor lightTextColor];
    view.clipsToBounds = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"co=%@, div=%@, cuno=%@, datfrom=%@, datto=%@, isclosed=%@", self.co, self.div, self.cuno, self.startDate, self.endDate, self.isclosed);
    [self preSetView:self.tableView];

    [self getNumberData];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - request data



-(void)getNumberData{
    NSString *urlString = [[NSString alloc] init];
    NSString *api_key = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
    NSLog(@"api = %@", api_key);
    
    urlString = [NSString stringWithFormat:TRANSPORT_URL2@"&DLDAT=%@&VHNO=%@",api_key,self.cuno, self.co, self.div, self.dldat, self.form_number];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSLog(@"%@", url);
    //[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    //[SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD show];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *urlResponse, NSData * data, NSError *error) {
                               
                               NSData * jsonData = [NSData dataWithContentsOfURL:url];
                               NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                               if ([dataDictionary isKindOfClass:[NSDictionary class]] && [dataDictionary[@"data"] count] != 0) {
                                   self.ListTableData = dataDictionary[@"data"];
                                   [self.tableView reloadData];
                                   //NSLog(@"numdata = %@", self.ListTableData);
                                   [SVProgressHUD dismiss];
                               }else if([dataDictionary[@"data"] count] == 0){
                                   NSLog(@"error");
                                   [SVProgressHUD dismiss];
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"查無清單" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"確認", nil];
                                   [alert show];
                               }
                               
                           }];
    
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.ListTableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NotiDetailTableViewCell *cell = (NotiDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors =  [NSArray arrayWithObjects:(id)[UIColor lightGrayColor].CGColor, (id)[UIColor whiteColor].CGColor, nil];
    [cell.layer insertSublayer:gradient below:0];
    
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void)configureCell:(NotiDetailTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *list = [[NSDictionary alloc] init];
    
    list = [self.ListTableData objectAtIndex:indexPath.row];
    
    cell.name.text = ([list valueForKey:@"CUABR"]!=[NSNull null])?[list valueForKey:@"CUABR"]:@"未知";
    
    
    cell.prograss.text = ([list valueForKey:@"state"]!=[NSNull null])?[list valueForKey:@"state"]:@"未知";
    [cell.prograss sizeToFit];
    [cell.prograss.layer setCornerRadius:4.0f];
    [cell.prograss setClipsToBounds:YES];
    
    if([cell.prograss.text isEqualToString:@"已出發"]){
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f]];
    }else if ([cell.prograss.text isEqualToString:@"已抵達轉運站"]){
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:240/255.0f green:173/255.0f blue:78/255.0f alpha:1.0f]];
    }else if ([cell.prograss.text isEqualToString:@"已部份抵達"]){
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:91/255.0f green:192/255.0f blue:222/255.0f alpha:1.0f]];
    }else if ([cell.prograss.text isEqualToString:@"已抵達"]){
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f]];
    }else if ([cell.prograss.text isEqualToString:@"異常結案"]){
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:217/255.0f green:83/255.0f blue:79/255.0f alpha:1.0f]];
    }else{
        [cell.prograss setBackgroundColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f]];
    }
    
    
    cell.hoster.text = ([list valueForKey:@"SALMNM"]!=[NSNull null])?[list valueForKey:@"SALMNM"]:@"未知";
    cell.number.text = ([list valueForKey:@"ODNO"]!=[NSNull null])?[list valueForKey:@"ODNO"]:@"未知";
    cell.number_extra.text = ([list valueForKey:@"VHNO"]!=[NSNull null])?[list valueForKey:@"VHNO"]:@"未知";
    cell.logisticName.text = ([list valueForKey:@"CARREBR"]!=[NSNull null])?[list valueForKey:@"CARREBR"]:@"未知";
    cell.locationTime.text = ([list valueForKey:@"locationTime"]!=[NSNull null])?[list valueForKey:@"locationTime"]:@"未知";
    
    
    self.location = [list valueForKey:@"latitude"];
    
    if (self.location!=(NSString *)[NSNull null]) {
        
        cell.Map.hidden = NO;
        [cell.Map addTarget:self action:@selector(customActionPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.Map.backgroundColor= [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        [[cell.Map layer] setBorderWidth:1.0f];
        [[cell.Map layer] setBorderColor:[UIColor clearColor].CGColor];
        cell.Map.layer.cornerRadius = 4;
    }else{
        cell.Map.hidden = YES;
    }
    
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"toDetailView" sender:self];
    
}

#pragma mark - IBAction

- (void)customActionPressed:(id)sender {
    
    self.mapIndex = [self GetIndexPathFromSender:sender];
    [self performSegueWithIdentifier:@"toMap" sender:self];
}




-(NSIndexPath*)GetIndexPathFromSender:(id)sender{
    
    if(!sender) { return nil; }
    
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        return [self.tableView indexPathForCell:cell];
    }
    
    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"toDetailView"]) {
        DetailViewController *vc = segue.destinationViewController;
        vc.detailData = [self.ListTableData objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        vc.detailCard = self.ListTableData;
        vc.indexnum = [self.tableView indexPathForSelectedRow];
        
    }else if([segue.identifier isEqualToString:@"toMap"]){
        NotiMapViewController *vc = segue.destinationViewController;
        NSDictionary *temp = [[NSDictionary alloc] init];
        temp = [self.ListTableData objectAtIndex:self.mapIndex.row];
        vc.latitude = [temp valueForKey:@"latitude"];
        vc.longitude = [temp valueForKey:@"longitude"];
        vc.address = [temp valueForKey:@"address"];
        vc.location_time = [temp valueForKey:@"locationTime"];
        vc.vhno = [temp valueForKey:@"VHNO"];
        vc.odno = [temp valueForKey:@"ODNO"];
        vc.state = [temp valueForKey:@"state"];
    }
     
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
