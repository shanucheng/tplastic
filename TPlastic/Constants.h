//
//  Constants.h
//  TPlastic
//
//  Created by chenghsienyu on 12/17/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

static NSString *sProjectName = @"Model";

/*測試用*/
/*
#define REGISTER_DEVICE @"http://mobile.free168.tw/fpc/api/device"
#define LOGIN_URL @"http://mobile.free168.tw/fpc//api/login"
#define TRANSPORT_URL @"http://mobile.free168.tw/fpc/api/transport/page/1?api-key=%@&CUNO=%@&AODAT_FROM=%@&AODAT_TO=%@&isclosed=%@&CO=%@&DIV=%@"
#define TRANSPORT_URL2 @"http://mobile.free168.tw/fpc/api/transport/page/1?api-key=%@&CUNO=%@&isclosed=all&CO=%@&DIV=%@"
#define LOCATION_POST @"http://mobile.free168.tw/fpc/api/location"
#define CUSTOMER_ID_URL @"http://mobile.free168.tw/fpc/api/customer/page/1?api-key=%@"
#define SEARCH_COS_URL @"http://mobile.free168.tw/fpc/api/transport/page/1"
#define NUMBER_URL @"http://mobile.free168.tw/fpc/api/formnumber/page/1?api-key=%@"
#define LOGOUT_URL @"http://mobile.free168.tw/fpc/api/logout"
#define APPLYING_URL @"http://mobile.free168.tw/fpc/registration"
*/
/*正式*/

#define REGISTER_DEVICE @"http://www.logistic.fpcetg.com.tw/api/device"
#define LOCATION_POST @"http://www.logistic.fpcetg.com.tw/api/location"
#define LOGIN_URL @"http://www.logistic.fpcetg.com.tw/api/login"
#define CUSTOMER_ID_URL @"http://www.logistic.fpcetg.com.tw/api/customer/page/1?api-key=%@"
#define SEARCH_COS_URL @"http://www.logistic.fpcetg.com.tw/api/transport/page/1"
#define TRANSPORT_URL @"http://www.logistic.fpcetg.com.tw/api/transport/page/1?api-key=%@&CUNO=%@&AODAT_FROM=%@&AODAT_TO=%@&isclosed=%@&CO=%@&DIV=%@"
#define TRANSPORT_URL2 @"http://www.logistic.fpcetg.com.tw/api/transport/page/1?api-key=%@&CUNO=%@&isclosed=all&CO=%@&DIV=%@"
#define NUMBER_URL @"http://www.logistic.fpcetg.com.tw/api/formnumber/page/1?api-key=%@"
#define LOGOUT_URL @"http://www.logistic.fpcetg.com.tw/api/logout"
#define APPLYING_URL @"http://www.logistic.fpcetg.com.tw/registration"
#define APP_VERSION_CHECK_URL @"http://itunes.apple.com/tw/lookup?id=984404233"

#define APP_ID @"876204723"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)