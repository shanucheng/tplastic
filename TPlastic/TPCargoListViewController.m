//
//  TPCargoListViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 12/8/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "QRScanViewController.h"
#import "TPCargoListViewController.h"
#import "CargoListTableViewCell.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "PostData.h"
#import "TempData.h"

@interface TPCargoListViewController () <NSFetchedResultsControllerDelegate>
{
    NSTimer* locationUpdateTimer;
    NSTimer* logoutTimer;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSIndexPath *selection;

@property (strong, nonatomic) NSString *location_interval;
@property (strong, nonatomic) NSString *force_logout_interval;


@end

@implementation TPCargoListViewController


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}



- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


-(void) preSetView:(UIView *)view{
    view.layer.cornerRadius = 5;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.backgroundColor = [UIColor lightTextColor];
    view.clipsToBounds = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self preSetView:self.TableView];
    self.confirm_btn.layer.masksToBounds = YES;
    self.confirm_btn.layer.cornerRadius = 5;
    self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    [self.selectState_btn sizeToFit];
    [self.selectState_btn.layer setBorderWidth:1.0f];
    [self.selectState_btn.layer setCornerRadius:4.0f];
    [self.selectState_btn.layer setBorderColor:[UIColor clearColor].CGColor];
    [self.selectState_btn setBackgroundColor:[UIColor blackColor]];
    
    [self setupData];
    
    
    
}

- (void) setupData{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PostData"];
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if (result.count != 0) {
        
        //NSLog(@"has PostData");
        for (NSDictionary *dict in result) {
            NSManagedObject *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"TempData" inManagedObjectContext:context];
            TempData *tempData = (TempData*)newObj;
            tempData.vhno = [dict valueForKey:@"vhno"];
            tempData.dldat = [dict valueForKey:@"dldat"];
            tempData.co = [dict valueForKey:@"co"];
            tempData.div = [dict valueForKey:@"div"];
            tempData.stateID = [dict valueForKey:@"stateID"];
            tempData.isChecked = [NSNumber numberWithBool:NO];
            tempData.valid = [NSNumber numberWithBool:YES];
        }
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        self.confirm_btn.enabled = YES;
        self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    }
    
    // perform fetch
    //Don't think we should actually be fetching during view load
    [[self fetchedResultsController] performFetch:&error];
    if (error) {
        NSLog(@"Error occured fetching data: %@", error);
    }
    
}


- (void)viewDidAppear:(BOOL)animated{
    /*
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TempData"];
    NSMutableArray *tempData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSLog(@"tempData = %@", tempData);
    */
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -
#pragma mark Fetched Results Controller Delegate Methods

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // create fetch request for demo entities
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TempData"  inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // set sort properties
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dldat" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                              managedObjectContext:context
                                                sectionNameKeyPath:nil
                                                         cacheName:nil];
    
    _fetchedResultsController = self.fetchedResultsController;
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.TableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.TableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.TableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.TableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(CargoListTableViewCell *)[self.TableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.TableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.TableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}


#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CargoListTableViewCell *cell = (CargoListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors =  [NSArray arrayWithObjects:(id)[UIColor lightGrayColor].CGColor, (id)[UIColor whiteColor].CGColor, nil];
    [cell.layer insertSublayer:gradient below:0];
    
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(CargoListTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    cell.state_button.enabled = NO;
    
    // Fetch Record
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Update Cell
    [cell.vhno_Label setText:[record valueForKey:@"vhno"]];
    [cell.dldat_Label setText:[record valueForKey:@"dldat"]];
    [cell.seleced_btn setSelected:[[record valueForKey:@"isChecked"] boolValue]];
    
    [cell.state_button sizeToFit];
    [cell.state_button.layer setBorderWidth:1.0f];
    cell.state_button.layer.cornerRadius = 4;
    [[cell.state_button layer] setBorderColor:[UIColor clearColor].CGColor];
    
    [cell setDidTapButtonBlock:^{
        
        //reset selectall btn
        [self.selectAll setSelected:NO];
        
        BOOL isDone = [[record valueForKey:@"isChecked"] boolValue];
        
        // Update Record
        [record setValue:@(!isDone) forKey:@"isChecked"];
    }];
    
    //NSLog(@"valid = %@", [record valueForKey:@"valid"]);
    
    if ([[record valueForKey:@"stateID"] isEqualToString:@"1"] && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"1"]){
        
        [cell.state_button setTitle:@"已出發" forState:UIControlStateNormal];
        cell.state_button.backgroundColor = [UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f];
        
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.selectState_btn.frame.size.height - 1.0f, self.selectState_btn.frame.size.width, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f];
        //[self.selectState_btn addSubview:bottomBorder];
        
        self.confirm_btn.enabled = YES;
        self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        
    }else if ([[record valueForKey:@"stateID"] isEqualToString:@"2"] && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"1"]){
        
        [cell.state_button setTitle:@"已抵達轉運站" forState:UIControlStateNormal];
        cell.state_button.backgroundColor = [UIColor colorWithRed:240/255.0f green:173/255.0f blue:78/255.0f alpha:1.0f];
        
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.selectState_btn.frame.size.height - 1.0f, self.selectState_btn.frame.size.width, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:240/255.0f green:173/255.0f blue:78/255.0f alpha:1.0f];
        //[self.selectState_btn addSubview:bottomBorder];
        
        self.confirm_btn.enabled = YES;
        self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        
    }else if ([[record valueForKey:@"stateID"] isEqualToString:@"3"] && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"1"]){
        
        [cell.state_button setTitle:@"已部份抵達" forState:UIControlStateNormal];
        cell.state_button.backgroundColor = [UIColor colorWithRed:91/255.0f green:192/255.0f blue:222/255.0f alpha:1.0f];
        
        
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.selectState_btn.frame.size.height - 1.0f, self.selectState_btn.frame.size.width, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:91/255.0f green:192/255.0f blue:222/255.0f alpha:1.0f];
        //[self.selectState_btn addSubview:bottomBorder];
        
        self.confirm_btn.enabled = YES;
        self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        
    }else if ([[record valueForKey:@"stateID"] isEqualToString:@"4"] && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"1"]){
        
        [cell.state_button setTitle:@"已送達" forState:UIControlStateNormal];
        cell.state_button.backgroundColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f];
        
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.selectState_btn.frame.size.height - 1.0f, self.selectState_btn.frame.size.width, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f];
        //[self.selectState_btn addSubview:bottomBorder];
        
        self.confirm_btn.enabled = YES;
        self.confirm_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        
    }else if ([[record valueForKey:@"stateID"] isEqualToString:@"0"] && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"0"]){
        
        [cell.state_button setTitle:@"" forState:UIControlStateNormal];
        cell.state_button.backgroundColor = [UIColor clearColor];
        
        
    }

}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (record && [[[record valueForKey:@"valid"] stringValue] isEqualToString:@"1"]) {
            //get newest location
            LocationShareModel *LS = [LocationShareModel sharedModel];
            NSString *latitude = [[LS.myLocationArray lastObject] valueForKey:@"latitude"];
            NSString *longitude = [[LS.myLocationArray lastObject] valueForKey:@"longitude"];
            //NSLog(@"%@,  %@", latitude, longitude);
            // get current date/time
            NSDate *currDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSString *dateString = [dateFormatter stringFromDate:currDate];
            
            //create Json temp
            NSMutableArray *tempList = [[NSMutableArray alloc] init];
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
            [tempDict setValue:[record valueForKey:@"co"] forKey:@"CO"];
            [tempDict setValue:[record valueForKey:@"vhno"] forKey:@"VHNO"];
            [tempDict setValue:[record valueForKey:@"dldat"] forKey:@"DLDAT"];
            [tempDict setValue:[record valueForKey:@"isChecked"] forKey:@"isChecked"];
            [tempDict setValue:[record valueForKey:@"div"] forKey:@"DIV"];
            [tempDict setValue:@"5" forKey:@"stateID"];
            [tempDict setValue:latitude forKey:@"latitude"];
            [tempDict setValue:longitude forKey:@"longitude"];
            [tempDict setValue:dateString forKey:@"locationTime"];
            
            [tempList addObject:tempDict];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempList
                                                               options:0
                                                                 error:nil];
            NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
            
            [self PostDeleteData:JSONString];
            
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
            
            NSManagedObjectContext *context = [self managedObjectContext];
            NSError *error = nil;
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PostData"];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"vhno == %@", [record valueForKey:@"vhno"]];
            fetchRequest.predicate = pred;
            NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
            
            for(NSManagedObject *obj in result){
                [context deleteObject:obj];
            }
            // Save the object to persistent store
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
            
            
        }else {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
        
    }
}

#pragma mark Table View Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Store Selection
    [self setSelection:indexPath];
    
    //reset select all btn
    
    [self.selectAll setSelected:NO];
    
    //set select btn
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([[record valueForKey:@"isChecked"] boolValue]) {
        [record setValue:[NSNumber numberWithBool:NO] forKey:@"isChecked"];
    }else{
        [record setValue:[NSNumber numberWithBool:YES] forKey:@"isChecked"];
    }

}

#pragma mark - postData

-(void) updatePostService{
    
    NSLog(@"Post in manual confirm");
    
    if (![self connected]) {
        // not connected
        //NSLog(@"no internet");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無網路連線"
                                                        message:@"請檢查網路連線狀態"
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
        
    } else {
        // connected, do some internet stuff
        //NSLog(@"has internet");
        
        //get apiKey
        NSString *apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
        NSString *JSONString = [self convertEntityToJson:@"PostData"];
        
        NSString *post =[[NSString alloc] initWithFormat:@"location=%@&api-key=%@",JSONString, apiKey];
        NSURL *url=[NSURL URLWithString:LOCATION_POST];
        
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
        [SVProgressHUD show];
        
        self.view.userInteractionEnabled = NO;
        
        dispatch_queue_t queue = dispatch_queue_create("com.freeking.TPlastic.queue", DISPATCH_QUEUE_CONCURRENT);
        
        dispatch_async(queue, ^{
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //NSLog(@"Response code: %ld", (long)[response statusCode]);
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([response statusCode] >= 200 && [response statusCode] < 300){
                    //NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    //NSLog(@"Response ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    if (jsonData[@"success"]) {
                        NSLog(@"result = %@", jsonData[@"result"]);
                        NSArray *result = jsonData[@"result"];
                        NSString *location_interval = [result[1] valueForKey:@"optionValue"];
                        NSString *force_logout_interval = [result[0] valueForKey:@"optionValue"];
                        
                        self.location_interval = location_interval;
                        self.force_logout_interval = force_logout_interval;
                        
                        [[NSUserDefaults standardUserDefaults] setValue:location_interval forKey:@"interval"];
                        [[NSUserDefaults standardUserDefaults] setValue:force_logout_interval forKey:@"logoutInterval"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        NSManagedObjectContext *context = [self managedObjectContext];
                        NSError *error = nil;
                        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PostData"];
                        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dldat" ascending:YES];
                        NSArray *sortDescriptor = [NSArray arrayWithObjects:sort, nil];
                        fetchRequest.sortDescriptors = sortDescriptor;
                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"stateID != 1"];
                        fetchRequest.predicate = pred;
                        
                        NSArray *postResult = [context executeFetchRequest:fetchRequest error:&error];
                        
                        for (NSManagedObject *obj in postResult){
                            [context deleteObject:obj];
                        }
                        
                        //reset isChecked value
                        fetchRequest.predicate = nil;
                        NSArray *surPost = [context executeFetchRequest:fetchRequest error:&error];
                        
                        for (PostData *postData in surPost){
                            postData.isChecked = [NSNumber numberWithBool:NO];
                        }
                        
                        
                        // Save the object to persistent store
                        if (![context save:&error]) {
                            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
                        }
                        
                        [self cleanScanData];
                        [self setupData];
                        [self CheckPostDataTimerSetup];
                        
                        //[self setupTimer];//setup bgPost
                        
                        [self.selectState_btn setTitle:@"請選擇" forState:UIControlStateNormal];
                        self.selectState_btn.backgroundColor = [UIColor blackColor];
                        
                        [SVProgressHUD dismiss];
                        
                        sleep(2);///
                        
                        
                    }else{
                        NSLog(@"result = %@", jsonData[@"error"]);
                        //刪除失敗 暫存至entity 無網路， 下次有網路時在使用nsurlsession upload by background fetch function
                        [SVProgressHUD dismiss];
                    }
                }
                self.view.userInteractionEnabled = YES;
                
            });
        });

    }
    
}



- (void)postDataInBG {
    
    NSLog(@"Post In Background");
    
    if (![self connected]) {
        // not connected
        //NSLog(@"no internet");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無網路連線"
                                                        message:@"請檢查網路連線狀態"
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
        
    } else {
        NSString *apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
        NSString *JSONString = [self convertEntityToJson:@"PostData"];
        
        NSString *post =[[NSString alloc] initWithFormat:@"location=%@&api-key=%@",JSONString, apiKey];
        NSURL *url=[NSURL URLWithString:LOCATION_POST];
        
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        
        dispatch_async(queue, ^{
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //NSLog(@"Response code: %ld", (long)[response statusCode]);
            dispatch_sync(dispatch_get_main_queue(), ^{
                    //NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    //NSLog(@"Response ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    if (jsonData[@"success"]) {
                        NSLog(@"result = %@", jsonData[@"result"]);
                        NSArray *result = jsonData[@"result"];
                        NSString *location_interval = [result[1] valueForKey:@"optionValue"];
                        NSString *force_logout_interval = [result[0] valueForKey:@"optionValue"];
                        
                        self.location_interval = location_interval;
                        self.force_logout_interval = force_logout_interval;
                        
                        [[NSUserDefaults standardUserDefaults] setValue:location_interval forKey:@"interval"];
                        [[NSUserDefaults standardUserDefaults] setValue:force_logout_interval forKey:@"logoutInterval"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                     
                        [self CheckPostDataTimerSetup];
                        
                    }
            });
        });
        
    }

}


- (NSString*)convertEntityToJson:(NSString*)entity{
    
    [self.locationTracker updateLocationToServer];//add temp
    
    //get newest location
    LocationShareModel *LS = [LocationShareModel sharedModel];
    NSString *latitude = [[[LS.myLocationArray lastObject] valueForKey:@"latitude"] stringValue];
    NSString *longitude = [[[LS.myLocationArray lastObject] valueForKey:@"longitude"] stringValue];
    NSLog(@"latitude%@, longitude%@", latitude, longitude);
    // get current date/time
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currDate];
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dldat" ascending:YES];
    NSArray *sortDescriptor = [NSArray arrayWithObjects:sort, nil];
    fetchRequest.sortDescriptors = sortDescriptor;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *tempList = [[NSMutableArray alloc] init];
    for (NSArray *obj in result) {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
        [tempDict setValue:[obj valueForKey:@"co"] forKey:@"CO"];
        [tempDict setValue:[obj valueForKey:@"vhno"] forKey:@"VHNO"];
        [tempDict setValue:[obj valueForKey:@"dldat"] forKey:@"DLDAT"];
        [tempDict setValue:[obj valueForKey:@"isChecked"] forKey:@"isChecked"];
        [tempDict setValue:[obj valueForKey:@"div"] forKey:@"DIV"];
        [tempDict setValue:[obj valueForKey:@"stateID"] forKey:@"stateID"];
        [tempDict setValue:latitude forKey:@"latitude"];
        [tempDict setValue:longitude forKey:@"longitude"];
        [tempDict setValue:dateString forKey:@"locationTime"];
        
        [tempList addObject:tempDict];
    }

    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempList
                                                       options:0
                                                         error:nil];
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    
    return JSONString;
}

- (void) cleanScanData{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for (NSManagedObject *record in [self.fetchedResultsController fetchedObjects]) {
        
        [context deleteObject:record];
        
    }
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

- (void)CheckPostDataTimerSetup{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"PostData"];
    NSArray *postResult = [context executeFetchRequest:fetchRequest error:&error];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (postResult.count == 0) {
            
            
            if (locationUpdateTimer) {
                [locationUpdateTimer invalidate];
                locationUpdateTimer = nil;
            }
            if (logoutTimer) {
                [logoutTimer invalidate];
                logoutTimer = nil;
            }
            
            [self.locationTracker stopLocationTracking];
            
        } else {
            
            
            NSLog(@"intervaltime = %@ , logouttime = %@", self.location_interval, self.force_logout_interval);
            
            //intervalTime setup for post
            NSTimeInterval time = 60 * self.location_interval.integerValue;
            
            locationUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:time
                                                                   target:self
                                                                 selector:@selector(postDataInBG)
                                                                 userInfo:nil
                                                                  repeats:NO];
            
            //intervalTime setup for logout location
            
            NSTimeInterval time_logout = 60 * 60 * self.force_logout_interval.integerValue;
            
            logoutTimer = [NSTimer scheduledTimerWithTimeInterval:time_logout
                                                           target:self
                                                         selector:@selector(stopLocation)
                                                         userInfo:nil
                                                          repeats:NO];
            
            
        }
    });
}


- (void) PostDeleteData:(NSString*)JSONString{
    
    //get apiKey
    NSString *apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
    
    NSString *post =[[NSString alloc] initWithFormat:@"location=%@&api-key=%@",JSONString, apiKey];
    
    //NSLog(@"PostData: %@",post);
    
    NSURL *url=[NSURL URLWithString:LOCATION_POST];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD show];
    self.view.userInteractionEnabled = NO;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSError *error = [[NSError alloc] init];
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //NSLog(@"Response code: %ld", (long)[response statusCode]);
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([response statusCode] >= 200 && [response statusCode] < 300){
                //NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                //NSLog(@"Response ==> %@", responseData);
                
                NSError *error = nil;
                NSDictionary *jsonData = [NSJSONSerialization
                                          JSONObjectWithData:urlData
                                          options:NSJSONReadingMutableContainers
                                          error:&error];
                if (jsonData[@"success"]) {
                    NSLog(@"result = %@", jsonData[@"result"]);
                    NSArray *result = jsonData[@"result"];
                    NSString *location_interval = [result[0] valueForKey:@"optionValue"];
                    NSString *force_logout_interval = [result[1] valueForKey:@"optionValue"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:location_interval forKey:@"interval"];
                    [[NSUserDefaults standardUserDefaults] setValue:force_logout_interval forKey:@"logoutInterval"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [SVProgressHUD dismiss];
                }else{
                    NSLog(@"result = %@", jsonData[@"error"]);
                    
                    //刪除失敗 暫存至entity 無網路， 下次有網路時在使用nsurlsession upload by background fetch function
                    [SVProgressHUD dismiss];
                }
            }
            self.view.userInteractionEnabled = YES;
        });
    });
    
    
    
    [self.locationTracker updateLocationToServer];
    
}

#pragma mark - alert index

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ([alertView.title isEqualToString:@"選擇運單狀態"]) {
        
        NSManagedObjectContext *context = [self managedObjectContext];
        NSError *error = nil;
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"TempData"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isChecked == 1"];
        NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
        
        
        switch (buttonIndex) {
            case 0:
                NSLog(@"已出發");
                
                [self.selectState_btn setTitle:@"已出發" forState:UIControlStateNormal];
                [self.selectState_btn setBackgroundColor:[UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f]];
                
                for(TempData *tempData in result){
                    tempData.stateID = @"1";
                    tempData.valid = [NSNumber numberWithBool:NO];
                }
                
                break;
                
            case 1:
                NSLog(@"已抵達轉運站");
                
                [self.selectState_btn setTitle:@"已抵達轉運站" forState:UIControlStateNormal];
                [self.selectState_btn setBackgroundColor:[UIColor colorWithRed:240/255.0f green:173/255.0f blue:78/255.0f alpha:1.0f]];
                
                for(TempData *tempData in result){
                    tempData.stateID = @"2";
                    tempData.valid = [NSNumber numberWithBool:NO];
                }
                
                break;
            case 2:
                NSLog(@"已部份抵達");
                
                [self.selectState_btn setTitle:@"已部份抵達" forState:UIControlStateNormal];
                [self.selectState_btn setBackgroundColor:[UIColor colorWithRed:91/255.0f green:192/255.0f blue:222/255.0f alpha:1.0f]];
                
                for(TempData *tempData in result){
                    tempData.stateID = @"3";
                    tempData.valid = [NSNumber numberWithBool:NO];
                }
                
                break;
            case 3:
                NSLog(@"已送達");
                
                [self.selectState_btn setTitle:@"已送達" forState:UIControlStateNormal];
                [self.selectState_btn setBackgroundColor:[UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f]];
                
                for(TempData *tempData in result){
                    tempData.stateID = @"4";
                    tempData.valid = [NSNumber numberWithBool:NO];
                }
                
                break;
                
            default:
                break;
        }
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
    }
    
    
}



#pragma mark - IBAction


- (IBAction)ToScanView:(id)sender {
    
    [self performSegueWithIdentifier:@"toScanView" sender:self];
    
}


- (IBAction)selectAll_btn:(id)sender {
    if (self.selectAll.selected) {
        [self.selectAll setSelected:NO];
        
        for (NSManagedObject *record in [self.fetchedResultsController fetchedObjects]) {
            [record setValue:[NSNumber numberWithBool:NO] forKey:@"isChecked"];
        }
    }else{
        [self.selectAll setSelected:YES];
        
        for (NSManagedObject *record in [self.fetchedResultsController fetchedObjects]) {
            [record setValue:[NSNumber numberWithBool:YES] forKey:@"isChecked"];
        }
        
    }
    
}

- (IBAction)selectState_btn:(id)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"TempData"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isChecked == 1"];
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    
    if (result.count != 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"選擇運單狀態"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"已出發", @"已抵達轉運站", @"已部份抵達", @"已送達", nil];
        
        
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請選擇運單"
                                                        message:@"勾選要改變狀態的運單"
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
}




- (IBAction)confirm_btn:(id)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"TempData"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isChecked == 1"];
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if (result.count != 0) {
        for (TempData *tempData in result) {
            tempData.valid = [NSNumber numberWithBool:YES];
            
        }
        

        
        
        for (NSDictionary *dict in result) {
            
            NSFetchRequest *postFetch = [NSFetchRequest fetchRequestWithEntityName:@"PostData"];
            postFetch.predicate = [NSPredicate predicateWithFormat:@"vhno == %@", [dict valueForKey:@"vhno"]];
            NSArray *postResult = [context executeFetchRequest:postFetch error:&error];
            
            if(postResult.count !=0){
                PostData *postData = [postResult objectAtIndex:0];
                postData.vhno = [dict valueForKey:@"vhno"];
                postData.dldat = [dict valueForKey:@"dldat"];
                postData.co = [dict valueForKey:@"co"];
                postData.div = [dict valueForKey:@"div"];
                postData.stateID = [dict valueForKey:@"stateID"];
                postData.isChecked = [dict valueForKey:@"isChecked"];

                
            }else {
                //NSLog(@"result = %@", [dict valueForKey:@"vhno"]);
                NSManagedObject *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"PostData" inManagedObjectContext:context];
                PostData *postData = (PostData*) newObj;
                postData.vhno = [dict valueForKey:@"vhno"];
                postData.dldat = [dict valueForKey:@"dldat"];
                postData.co = [dict valueForKey:@"co"];
                postData.div = [dict valueForKey:@"div"];
                postData.stateID = [dict valueForKey:@"stateID"];
                postData.isChecked = [dict valueForKey:@"isChecked"];

            }
            
        }
        
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        dispatch_queue_t queue = dispatch_queue_create("com.freeking.TPlastic.queue", DISPATCH_QUEUE_CONCURRENT);
        
        dispatch_async(queue, ^{
            NSLog(@"Post Data");
            [self updatePostService];
            sleep(4);
        });
        
        dispatch_barrier_async(queue, ^{
            NSLog(@"success");
        
        });
        

    }else {
        [self.confirm_btn setBackgroundColor:[UIColor grayColor]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請掃描加入清單"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
    
}


- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
        NSLog(@"Back pressed");
        [self cleanScanData];
    }
}



#pragma mark - stop location service

- (void)stopLocation{
    NSLog(@"stop location service");
    [self.locationTracker stopLocationTracking];
    if (locationUpdateTimer) {
        [locationUpdateTimer invalidate];
        locationUpdateTimer = nil;
    }
    if (logoutTimer) {
        [logoutTimer invalidate];
        logoutTimer = nil;
    }
}



@end
