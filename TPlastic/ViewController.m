//
//  ViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 12/8/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"功能選擇";
    
    self.toCargo_btn.layer.masksToBounds = YES;
    self.toCargo_btn.layer.cornerRadius = 5;
    self.toCargo_btn.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.toSearch.layer.masksToBounds = YES;
    self.toSearch.layer.cornerRadius = 5;
    self.toSearch.backgroundColor = [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
    
    self.bundle_ver.text = [NSString stringWithFormat:@"Version. %@", [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - segue_perform
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toCargoList"]) {
        //NSLog(@"toCargoList");
    }else if([segue.identifier isEqualToString:@"toLoginView"]){
        
    }
}


#pragma mark - Btn_Action
- (IBAction)toCargo:(id)sender {
    
    [self performSegueWithIdentifier:@"toCargoList" sender:self];
    
}

- (IBAction)toLoginView_btn:(id)sender {

    [self performSegueWithIdentifier:@"toLoginView" sender:self];

}


@end
