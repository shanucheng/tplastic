//
//  SLDetailViewController.h
//  CRMFPG
//
//  Created by chenghsienyu on 17/11/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@interface SLDetailViewController : UIViewController

@property (nonatomic, strong) NSArray *list_data;
@property (nonatomic, strong) NSDictionary *detailData;
@property (nonatomic, strong) NSArray *detailCard;
@property (nonatomic, strong) NSIndexPath *indexnum;

@end
