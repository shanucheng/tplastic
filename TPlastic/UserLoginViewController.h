//
//  UserLoginViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 20/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface UserLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *userID_textfield;
@property (weak, nonatomic) IBOutlet UITextField *pwd_textfield;
@property (weak, nonatomic) IBOutlet UIButton *remPwd_btn;
@property (weak, nonatomic) IBOutlet UIButton *autoLogin_btn;
@property (weak, nonatomic) IBOutlet UIButton *login_btn;
@property (weak, nonatomic) IBOutlet UIButton *register_btn;

- (BOOL)connected;
@end
