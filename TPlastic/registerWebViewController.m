//
//  registerWebViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 21/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "registerWebViewController.h"

@interface registerWebViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end

@implementation registerWebViewController

-(UIActivityIndicatorView *) spinner{
    if (!_spinner) {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _spinner.center = CGPointMake(self.webView.bounds.size.width/2, self.webView.bounds.size.height/2);
    }
    return _spinner;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationBar.title = @"台塑交運系統註冊";
    
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    [self.webView addSubview:self.spinner];
    [self.spinner startAnimating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UIWebView Delegate
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
    [self.spinner removeFromSuperview];
    self.spinner = nil;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
}


- (IBAction)backToPrevious_btn:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
