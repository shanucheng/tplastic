//
//  NotificationViewController.m
//  TPlastic
//
//  Created by chenghsienyu on 11/02/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "Notification.h"
#import "NotiMapViewController.h"
#import "NotiDetailViewController.h"

@interface NotificationViewController ()
{
    NSArray *tableList;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;

@end

@implementation NotificationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"推播訊息";
    [self customSetup];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    // get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents path
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to our Data/plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"notification.plist"];
    //This copies objects of plist to array if there is one
    [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
    NSLog(@"list  = %@", array);
    tableList = array;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem setTarget: self.revealViewController];
        [self.revealButtonItem setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        // Set the gesture
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NotificationTableViewCell *cell = (NotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors =  [NSArray arrayWithObjects:(id)[UIColor lightGrayColor].CGColor, (id)[UIColor whiteColor].CGColor, nil];
    [cell.layer insertSublayer:gradient below:0];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(NotificationTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //NSLog(@"listItem = %@", tableList);
    cell.vhno.text = [[[tableList objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"VN"];
    cell.odno.text = [[[tableList objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"ON"];
    cell.aodat.text = [[[tableList objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"date"];
    NSString *ID = [[[tableList objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"sID"];
    [cell.stateID sizeToFit];
    [cell.stateID.layer setCornerRadius:4.0f];
    [cell.stateID setClipsToBounds:YES];
    
    
    
    if([ID isEqualToString:@"1"]){
        [cell.stateID setBackgroundColor:[UIColor colorWithRed:5/255.0f green:203/255.0f blue:133/255.0f alpha:1.0f]];
        cell.stateID.text = @"已出發";
    }else if ([ID isEqualToString:@"4"]){
        [cell.stateID setBackgroundColor:[UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f]];
        cell.stateID.text = @"已抵達";
    }
    
    if ([[[[tableList objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"lat"] isEqual:[NSNull null]]) {
        [cell.map_btn setEnabled:NO];
    }else{
        cell.map_btn.backgroundColor= [UIColor colorWithRed:7/255.0f green:160/255.0f blue:255/255.0f alpha:1.0f];
        [[cell.map_btn layer] setBorderWidth:1.0f];
        [[cell.map_btn layer] setBorderColor:[UIColor clearColor].CGColor];
        cell.map_btn.layer.cornerRadius = 4;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        // get paths from root direcory
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        // get documents path
        NSString *documentsPath = [paths objectAtIndex:0];
        // get the path to our Data/plist file
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"notification.plist"];
        //This copies objects of plist to array if there is one
        [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
        
        [array removeObject:[array objectAtIndex:indexPath.row]];
        
        [array writeToFile:plistPath atomically: TRUE];
        
        tableList = array;
        
        [self.tableView reloadData];
        
        
    }
}

#pragma mark Table View Delegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"did selected");
    [self performSegueWithIdentifier:@"toDetail" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"toMap"]) {
        NotiMapViewController *vc = segue.destinationViewController;
        vc.vhno = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0] valueForKey:@"VN"];
        vc.odno = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"ON"];
        vc.latitude = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0 ]valueForKey:@"lat"];
        vc.longitude = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"lon"];
        vc.location_time = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:1] valueForKey:@"date"];
        if ([[[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"sID"] isEqualToString:@"1"]) {
            vc.state = @"已出發";
        }else{
            vc.state = @"已抵達";
        }
        
    }else if ([segue.identifier isEqualToString:@"toDetail"]){
        NotiDetailViewController *vc = segue.destinationViewController;
        vc.cuno = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"CN"];
        vc.div = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"DIV"];
        vc.form_number = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"VN"];
        vc.dldat = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"DD"];
        vc.co = [[[tableList objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectAtIndex:0]valueForKey:@"CO"];
        
        
    }
}


- (IBAction)NotificationSetting:(id)sender {
    
    [self performSegueWithIdentifier:@"toSetting" sender:self];
    
}

@end
