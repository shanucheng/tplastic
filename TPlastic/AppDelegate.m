//
//  AppDelegate.m
//  TPlastic
//
//  Created by chenghsienyu on 12/8/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "AppDelegate.h"
#import "SSKeychain.h"
#import "SVProgressHUD.h"
#import "Reachability.h" // add new fuction later
#import "Constants.h"
#import "Notification.h"
#import "NotificationViewController.h"
#import "AGPushNoteView.h"
#import "MAPViewController.h"
#import "NotificationViewController.h"

@interface AppDelegate ()
{
    
    NSDictionary *info;
    
}

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - check internet

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - UUID and SSKeychain
- (void)generateUUID{
    //UUID
    NSString *retrieveuuid = [SSKeychain passwordForService:@"com.freeking.TPlastic"account:@"uuid"];
    //NSLog(@"retrieveuuid = %@", retrieveuuid);
    [[NSUserDefaults standardUserDefaults] setValue:retrieveuuid forKey:@"uuid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ( retrieveuuid == nil || [retrieveuuid isEqualToString:@""])
        
    {
        
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        
        assert(uuid != NULL);
        
        CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
        
        retrieveuuid = [NSString stringWithFormat:@"%@", uuidStr];
        
        //NSLog(@"new retrieveuuid = %@", retrieveuuid);
        
        [SSKeychain setPassword: retrieveuuid
         
                     forService:@"com.freeking.TPlastic"account:@"uuid"];
        
        [[NSUserDefaults standardUserDefaults] setValue:retrieveuuid forKey:@"uuid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


#pragma mark - application start

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [NSThread sleepForTimeInterval:2.0];  
    
    //UUID setup
    [self generateUUID];
    
    //-- Set Notification

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
    //Right, that is the point
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                         |UIRemoteNotificationTypeSound
                                                                                         |UIRemoteNotificationTypeAlert) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
    //register to receive notifications
    
    UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
    NSLog(@"api = %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"]);
    
    [self location_notification];
    
    
    //test
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    NSLog(@"%@", notification);
    
    if (notification)
    {
        [AGPushNoteView showWithNotificationMessage:notification.alertBody];
    }
    
    // Remove the badge number (this should be better in the applicationWillEnterForeground: method)
    application.applicationIconBadgeNumber = 0;
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self saveContext];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];

}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark Remote Notification

#ifdef __IPHONE_8_0

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if (application.applicationState == UIApplicationStateInactive)
    {
        // case 2
        
        if (notification.userInfo[@"lat"] != [NSNull null]) {
            
            NSLog(@"gp to map");
            
            
            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            MAPViewController *vc = (MAPViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"mapView"];
            vc.latitude = notification.userInfo[@"lat"];
            vc.longitude = notification.userInfo[@"lon"];
            vc.vhno = notification.userInfo[@"VN"];
            vc.odno = notification.userInfo[@"ON"];
            vc.location_time = notification.userInfo[@"DD"];
            if ([notification.userInfo[@"sID"] isEqualToString:@"1"]) {
                vc.state = @"已出發";
            }else{
                vc.state = @"已抵達";
            }
            [navigationController pushViewController:vc animated:YES];
            
            
        }else{
            NSLog(@"go to noti");
            
            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            NotificationViewController *vc = (NotificationViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"notiView"];
            [navigationController pushViewController:vc animated:YES];
        }
        
    }
    else if (application.applicationState == UIApplicationStateActive)
    {
        // case 3
        [AGPushNoteView showWithNotificationMessage:notification.alertBody];
        [AGPushNoteView setMessageAction:^(NSString *message) {
            if (notification.userInfo[@"lat"] != [NSNull null]) {
                
                NSLog(@"gp to map");
                
                
                UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                MAPViewController *vc = (MAPViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"mapView"];
                vc.latitude = notification.userInfo[@"lat"];
                vc.longitude = notification.userInfo[@"lon"];
                vc.vhno = notification.userInfo[@"VN"];
                vc.odno = notification.userInfo[@"ON"];
                vc.location_time = notification.userInfo[@"DD"];
                if ([notification.userInfo[@"sID"] isEqualToString:@"1"]) {
                    vc.state = @"已出發";
                }else{
                    vc.state = @"已抵達";
                }
                [navigationController pushViewController:vc animated:YES];
                
                
            }else{
                NSLog(@"go to noti");
                
                UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                NotificationViewController *vc = (NotificationViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"notiView"];
                [navigationController pushViewController:vc animated:YES];
                
            }
        }];
        
    }
    
    application.applicationIconBadgeNumber = notification.applicationIconBadgeNumber - 1;
    
    NSLog(@"%@", notification);
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSString *notification_state = [[NSUserDefaults standardUserDefaults] valueForKey:@"notification"];
    NSString *state_start = [[NSUserDefaults standardUserDefaults] valueForKey:@"state_start"];
    NSString *state_arrival = [[NSUserDefaults standardUserDefaults] valueForKey:@"state_arrival"];
    NSLog(@"not %@, st %@, ar %@", notification_state, state_start, state_arrival);
    
    if ([notification_state isEqualToString:@"on"]) {
        
        if ([state_start isEqualToString:@"on"]&&[state_arrival isEqualToString:@"on"]) {
            // Save the object to persistent store
            NSMutableArray *array = [[NSMutableArray alloc] init];
            // get paths from root direcory
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            // get documents path
            NSString *documentsPath = [paths objectAtIndex:0];
            // get the path to our Data/plist file
            NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"notification.plist"];
            //This copies objects of plist to array if there is one
            [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
            //getTime
            NSString *currentData;
            NSDate *now = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"];
            currentData = [dateFormatter stringFromDate:now];
            //NSLog(@"currentDataTime = %@", currentData);
            
            NSMutableArray *tempArray = [NSMutableArray array];
            NSMutableDictionary *tempdict = [NSMutableDictionary dictionary];
            [tempdict setValue:currentData forKey:@"date"];
            [tempArray addObject:userInfo];
            [tempArray addObject:tempdict];
            [array addObject:tempArray];
            
            [array writeToFile:plistPath atomically: TRUE];
            
            info = userInfo;
            
            [self fireLocalNotification];
        }else if([state_start isEqualToString:@"on"]&&[state_arrival isEqualToString:@"off"]){
            if ([userInfo[@"sID"] isEqualToString:@"1"]) {
                // Save the object to persistent store
                NSMutableArray *array = [[NSMutableArray alloc] init];
                // get paths from root direcory
                NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                // get documents path
                NSString *documentsPath = [paths objectAtIndex:0];
                // get the path to our Data/plist file
                NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"notification.plist"];
                //This copies objects of plist to array if there is one
                [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
                
                //getTime
                NSString *currentData;
                NSDate *now = [NSDate date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"];
                currentData = [dateFormatter stringFromDate:now];
                //NSLog(@"currentDataTime = %@", currentData);
                
                NSMutableArray *tempArray = [NSMutableArray array];
                NSMutableDictionary *tempdict = [NSMutableDictionary dictionary];
                [tempdict setValue:currentData forKey:@"date"];
                [tempArray addObject:userInfo];
                [tempArray addObject:tempdict];
                [array addObject:tempArray];
                
                //[array addObject:userInfo];
                
                [array writeToFile:plistPath atomically: TRUE];
                
                info = userInfo;
                
                [self fireLocalNotification];
            }
            
        }else if([state_arrival isEqualToString:@"on"] && [state_start isEqualToString:@"off"]){
            if ([userInfo[@"sID"] isEqualToString:@"4"]) {
                // Save the object to persistent store
                NSMutableArray *array = [[NSMutableArray alloc] init];
                // get paths from root direcory
                NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                // get documents path
                NSString *documentsPath = [paths objectAtIndex:0];
                // get the path to our Data/plist file
                NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"notification.plist"];
                //This copies objects of plist to array if there is one
                [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
                
                //getTime
                NSString *currentData;
                NSDate *now = [NSDate date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"];
                currentData = [dateFormatter stringFromDate:now];
                //NSLog(@"currentDataTime = %@", currentData);
                
                NSMutableArray *tempArray = [NSMutableArray array];
                NSMutableDictionary *tempdict = [NSMutableDictionary dictionary];
                [tempdict setValue:currentData forKey:@"date"];
                [tempArray addObject:userInfo];
                [tempArray addObject:tempdict];
                [array addObject:tempArray];
                
                //[array addObject:userInfo];
                
                [array writeToFile:plistPath atomically: TRUE];
                
                info = userInfo;
                
                [self fireLocalNotification];
            }
        }
        
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
    
    
 }

#endif

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)token
{
    
    if (![self connected]) {
        // not connected
        //NSLog(@"no internet");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無網路連線"
                                                        message:@"請檢查網路連線狀態"
                                                       delegate:self
                                              cancelButtonTitle:@"確定"
                                              otherButtonTitles:nil, nil];
        
        [alert show];
    }else{
    
        [self checkAppVersion];
    //NSLog(@"Device token: %@", [token description]);
    NSString *deviceID = [[token description] substringWithRange:NSMakeRange(1, [[token description] length]-2)];
    deviceID = [deviceID stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceID = [deviceID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //NSLog(@"Device token: %@", deviceID);
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceID forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"uuid"];
    //NSLog(@"token = %@, uuid = %@", deviceID, uuid);
    
    NSString * apiKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"];
    NSLog(@"api = %@", apiKey);
    if ( apiKey == nil || [apiKey isEqualToString:@""]){
        
        //驗證裝置取得apikey
        NSString *post =[[NSString alloc] initWithFormat:@"uuid=%@&token=%@&device=%@",uuid, deviceID,@"iOS"];
        
        NSLog(@"PostData: %@",post);
        
        NSURL *url=[NSURL URLWithString:REGISTER_DEVICE];
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
        [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD show];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //NSLog(@"Response code: %ld", (long)[response statusCode]);
            dispatch_sync(dispatch_get_main_queue(), ^{
                if ([response statusCode] >= 200 && [response statusCode] < 300){
                    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                    NSLog(@"Response ==> %@", responseData);
                    
                    NSError *error = nil;
                    NSDictionary *jsonData = [NSJSONSerialization
                                              JSONObjectWithData:urlData
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
                    if (jsonData[@"success"]) {
                        NSString *apikey = jsonData[@"result"];
                        [[NSUserDefaults standardUserDefaults] setValue:apikey forKey:@"apiKey"];
                        [[NSUserDefaults standardUserDefaults] setValue:@"on" forKey:@"notification"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSLog(@"new api key = %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"apiKey"]);
                        [SVProgressHUD dismiss];
                        
                    }else{
                        NSLog(@"result = %@", jsonData[@"error"]);
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"apiKey"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeNone];
                        
                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
                            //Right, that is the point
                            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                                                 |UIRemoteNotificationTypeSound
                                                                                                                 |UIRemoteNotificationTypeAlert) categories:nil];
                            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
                        } else {
                            //register to receive notifications
                            
                            UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
                            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
                        }
                        
                    }
                }else{
                    
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"apiKey"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeNone];
                    
                    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
                        //Right, that is the point
                        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                                             |UIRemoteNotificationTypeSound
                                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
                        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
                    } else {
                        //register to receive notifications
                        
                        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
                        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
                    }
                    
                }
                
            });
        });

        
    }
    }
    
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSError *err;
    NSLog(@"Error in registration. Error: %@", err);
    
}


#pragma mark - notificationUserMode

- (void)location_notification{
    UIAlertView * alert;
    
    //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied){
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"本應用程式需要啟用背景執行. To turn it on, go to Settings > General > Background App Refresh"
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"The functions of this app are limited because the Background App Refresh is disable."
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    }else{
        self.locationTracker = [[LocationTracker alloc]init];
        [self.locationTracker startLocationTracking];
    }
}

- (void) fireLocalNotification{
    // Fire notification immediately after 1 sec
    
    NSLog(@"localInfo = %@", info);
    NSString *msg;
    if ([info[@"sID"] isEqualToString:@"1"]) {
        msg = [NSString stringWithFormat:@"訂單號：%@ 運單號：%@ , 已出發。", info[@"ON"], info[@"VN"]];
    }else{
        msg = [NSString stringWithFormat:@"訂單號：%@ 運單號：%@ , 已抵達。", info[@"ON"], info[@"VN"]];
    }
    
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.userInfo = info;
    notif.alertBody = msg;
    notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber += 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
}




-(void) checkAppVersion{
    //利用非同步方法送出 GET 要求
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:APP_VERSION_CHECK_URL]];
    NSURLRequest *urlReq = [NSURLRequest requestWithURL:url];
    dispatch_queue_t sendDeviceTokenQueue = dispatch_queue_create("Send The Device Token", NULL);
    dispatch_async(sendDeviceTokenQueue, ^{
        NSData *returnData = [NSURLConnection sendSynchronousRequest:urlReq returningResponse:nil error:nil];
        //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        //NSLog(@"Provider returns: %@", returnString);
        if (!returnData) {
            return;
        }
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableContainers error: &error];
        if ([jsonObj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *jsonDict = (NSDictionary *) jsonObj;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *storeVersion = [[[jsonDict objectForKey:@"results"] firstObject] objectForKey:@"version"];
                NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                //NSLog(@"Store Ver: %@;  App Ver: %@", storeVersion, currentVersion);
                if ([storeVersion doubleValue] > [currentVersion doubleValue]) {
                    UIAlertView *alertview =[[UIAlertView alloc] initWithTitle:@"有更新版本 App 可以下載！"
                                                                       message:[NSString stringWithFormat:@"前往 App Store 下載【台塑交運動態】%@ 版？ (目前版本為 %@)", storeVersion, currentVersion]
                                                                      delegate:self
                                                             cancelButtonTitle:@"取消" otherButtonTitles:@"確認", nil];
                    [alertview show];
                }
            });
        }
    });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if ([alertView.title isEqualToString:@"有更新版本 App 可以下載！"]){
        switch(buttonIndex) {
            case 0:
                break;
            case 1:
                [self openAppStoreToUpdateApp];
                break;
            default:
                break;
        }
    }
}

-(void) openAppStoreToUpdateApp{
    SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
    storeProductViewController.delegate =self;
    [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier : APP_ID} completionBlock:^(BOOL result, NSError *error) {
        if (error) {
            NSLog(@"Error %@ with User Info %@.", error, [error userInfo]);
        } else {
            // Present Store Product View Controller
            [self.window.rootViewController presentViewController:storeProductViewController animated:YES completion:^{
            }];
        }
    }];
}

@end
