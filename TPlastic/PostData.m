//
//  PostData.m
//  TPlastic
//
//  Created by chenghsienyu on 11/02/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import "PostData.h"


@implementation PostData

@dynamic co;
@dynamic div;
@dynamic dldat;
@dynamic isChecked;
@dynamic latitude;
@dynamic locationTime;
@dynamic longitude;
@dynamic stateID;
@dynamic vhno;

@end
