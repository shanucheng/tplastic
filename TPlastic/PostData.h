//
//  PostData.h
//  TPlastic
//
//  Created by chenghsienyu on 11/02/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PostData : NSManagedObject

@property (nonatomic, retain) NSString * co;
@property (nonatomic, retain) NSString * div;
@property (nonatomic, retain) NSString * dldat;
@property (nonatomic, retain) NSNumber * isChecked;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * locationTime;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * stateID;
@property (nonatomic, retain) NSString * vhno;

@end
