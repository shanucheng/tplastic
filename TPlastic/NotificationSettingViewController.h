//
//  NotificationSettingViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 09/03/15.
//  Copyright (c) 2015 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface NotificationSettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *noti_switch;
@property (weak, nonatomic) IBOutlet UISwitch *state_start;
@property (weak, nonatomic) IBOutlet UISwitch *state_arrival;
@property (weak, nonatomic) IBOutlet UISwitch *vibrate;

@end
