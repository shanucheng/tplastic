//
//  customerID.m
//  TPlastic
//
//  Created by chenghsienyu on 23/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import "customerID.h"

@implementation customerID

@synthesize cuabr;
@synthesize cuno;
@synthesize DivName;
@synthesize co;
@synthesize div;

+ (id)sharedManager {
    static customerID *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init {
    if (self = [super init]) {
        cuabr = [NSString alloc];
        cuno = [NSString alloc];
        DivName = [NSString alloc];
        co = [NSString alloc];
        div = [NSString alloc];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
