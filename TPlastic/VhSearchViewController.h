//
//  VhSearchViewController.h
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPGTextField.h"

@interface VhSearchViewController : UIViewController <UITextFieldDelegate, MPGTextFieldDelegate>
{
    NSMutableArray *data;

}

@property (weak, nonatomic) IBOutlet MPGTextField *search_textField;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *api_key;


@end
