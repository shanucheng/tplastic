//
//  shareInfo.h
//  TPlastic
//
//  Created by chenghsienyu on 22/12/14.
//  Copyright (c) 2014 chenghsienyu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shareInfo : NSObject
{
    NSDictionary *userInfo;
}


@property (nonatomic, retain) NSDictionary *userInfo;


+ (id)sharedManager;

@end
